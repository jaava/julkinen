/*
 * c++ course project triplane2
 * terrain.h
 *	written by jaakko vallinoja
 */
#ifndef CPP_PROJECT_TERRAIN
#define CPP_PROJECT_TERRAIN
 
#include "defines.h"
#include "gameobject.h"
 
class Terrain: public GameObject
{
public:
	Terrain(size_t id, int type, int _faction, CL_String name, CL_Pointf pos): GameObject(id, type, pos) {
		faction = _faction;
		alive = true;
		angle = CL_Angle(0, cl_degrees);
		if (name == "Terrain1") {
			sprite_id = TERRAIN_SPRITE;
			outline = new CL_CollisionOutline("graphics/sand.png", accuracy_high);
		}
		else {
			sprite_id = TERRAIN_SPRITE2;
			outline = new CL_CollisionOutline("graphics/sand2.png", accuracy_high);
		}
		outline->set_alignment(origin_bottom_left);
		outline->set_translation(pos.x, pos.y);
		is_active = true;
	}
	~Terrain() {delete outline;}
	
	void update () {
		//does nothing atm
	}
	
	void doCollide(GameObject* obj) {
		//does nothing atm
	}
	
};

#endif
