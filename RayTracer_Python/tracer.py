
#raytracer python version
#by jaava

from __future__ import print_function

from defines import *
from objects import *
from vektor import *
from math import *

import pygame#for drawing an image file



class Tracer:

    def __init__(self):
        self.picture = []

        for i in range(SIZE):
            self.picture.append([])
            for j in range(SIZE):
                self.picture[i].append(Vektor(0,0,0))
        error = False
        for i in range(SIZE):
            if len(self.picture[i]) != SIZE:
                error = True
            if error:
                print("error in picture init")

        self.thelist = []
        self.lights = []
        self.initobj()




#this is the real shit
    def run(self):

        print("run")
        
        camera = Vektor(SIZE/2.0,SIZE/2.0, -DISTANCE)
        screen = Vektor(0,0,0)
    
        for i in range(SIZE):
			print(i)
            for j in range(SIZE):
                cvec = Vektor(i,j,0).minus(camera)
                cvec.norm()
				
				#implemented so that the intersect calls the diffuse that calls the shadows
				#that calls the reflection that if required recursively calls the intersect again
                self.picture[i][j] = self.intersect(cvec, camera)
    
        self.drawimage()
			

	#this creates objects. should be implemented to read an xml file but not done yet.
	#now just hardcoded objects
    def initobj(self):
        #BALLS
        newball = Ball(BALL, Vektor(250,460,800),160,Material(Vektor(250,45,50), 0.7))
        self.thelist.append(newball)
        
        newball = Ball(BALL, Vektor(-50,495,900),120,Material(Vektor(50,45,250), 0.6))
        self.thelist.append(newball)
        
        newball = Ball(BALL, Vektor(420,560,600),50,Material(Vektor(50,245,50), 0.5))
        self.thelist.append(newball)
        
        newball = Ball(BALL, Vektor(450,370,1200),250,Material(Vektor(150,95,120), 0.5))
        self.thelist.append(newball)
     
     
        
        
        #PLANES
        newplane = Plane(PLANE, Vektor(0,500,0), Vektor(0,-1,0), Material(Vektor(90,105,105)))
        self.thelist.append(newplane)
        
        newplane = Plane(PLANE, Vektor(0,500,-1500), Vektor(0,0,1), Material(Vektor(90,105,105)))
        self.thelist.append(newplane)
        

        
        
        
        
        #LIGHTS
        self.lights.append(Light(Vektor(1750,-1000,-100), 0.8))
        self.lights.append(Light(Vektor(-1150,-1000,-200), 0.6))
    
        #create white balls for lights
		#mostly for positioning debugging when the lights sources are not yet visible
        for l in self.lights:
            self.thelist.append(Ball(BALL, l.pos, 120.0*l.pow ,Material(Vektor(255,255,255),0,1,0,1)))
    
    
#INTERSECT---INTERSECT---INTERSECT---INTERSECT
    def intersect(self, cvec, cam, sobj = None):
        nearest = Vektor(1000000,1000000,1000000)
        neo = None #nearest object
        for obj in self.thelist:
            if obj != sobj:
                if obj.type == BALL:
                    a = cvec.x*cvec.x + cvec.y*cvec.y + cvec.z*cvec.z
                    b = 2*cvec.x*(cam.x-obj.pos.x) +  2*cvec.y*(cam.y-obj.pos.y) +  2*cvec.z*(cam.z-obj.pos.z)
                    c = obj.pos.x*obj.pos.x + obj.pos.y*obj.pos.y + obj.pos.z*obj.pos.z + cam.x*cam.x + cam.y*cam.y + cam.z*cam.z - 2*(obj.pos.x*cam.x + obj.pos.y*cam.y + obj.pos.z*cam.z) - obj.r*obj.r

                    disc = b*b - 4*a*c
                    if disc >= 0:
                        #if negative no intersection
                        t = (-b - sqrt(disc))/(2.0*a)
                        
                        if t > 0:
                        #if negative intersection behind the cam
                            intr = Vektor(cam.x+t*cvec.x, cam.y+t*cvec.y, cam.z+t*cvec.z)
                    
                    
                            if cam.distance(intr) < cam.distance(nearest):
                                nearest = intr
                                neo = obj
                    
                    
                    
                        
                elif obj.type == PLANE:
                    abc = obj.s
                    abc.norm()
                    d = obj.pos.distance(cam)
                
                    disc = abc.dotpr(cvec)
                    if disc != 0:
                        #if 0 plane and ray same direction -> no intersection
                        t = -(abc.dotpr(cam)+d) / disc
                        if t > 0:
                            #negative t means intersection behind the camera
                            if disc < 0:
                                intr = cam.plus(cvec.scapr(t))
                            
                            
                            else:
                                mcvec = cvec.scapr(-1)
                                intr = cam.plus(mcvec.scapr(t))
                        
                            if cam.distance(intr) < cam.distance(nearest):
                                nearest = intr
                                neo = obj
                                
    
    
        if neo != None:
            c = self.diffuse(nearest, neo, cam) #returns the color
            return c
            #default color almost black
        return Vektor(5,5,5)
        
      

#DIFFUSION---DIFFUSION---DIFFUSION---DIFFUSION
    def diffuse(self, p, obj, cam):
        
        sumvek = Vektor(0,0,0)
        
        lamps = len(self.lights)
        
        if obj.type == BALL and obj.mat.lig == 1:
            return Vektor(255,255,255)#if light
        
        elif obj.type == BALL and obj.mat.lig != 1:
            for l in self.lights:
            
                N = Vektor((p.x - obj.pos.x)/obj.r, (p.y - obj.pos.y)/obj.r, (p.z - obj.pos.z)/obj.r) #normal unit vector for a sphere

                L = Vektor(l.pos.x - p.x, l.pos.y - p.y, l.pos.z - p.z) #vector to light
                L.norm()

                fac = N.dotpr(L)
            
            
                #distance to light
                dtl = p.distance(l.pos)
                dtl /= 500.0
                fac = fac * 70.0/(dtl*dtl)
                
                
                if fac < 0:
                    fac = 0
                #print (fac)
            
                c = obj.mat.col
                dc = Vektor(KD*fac*c.x, KD*fac*c.y, KD*fac*c.z)
                dc = dc.scapr(l.pow) #lamp power
                
                
                if self.shadows(p,l, obj):
                    dc = dc.scapr(SHADOWCOEF)
                
                sumvek = sumvek.plus(dc)
                
                sumvek = sumvek.plus(Vektor(KA*c.x, KA*c.y, KA*c.z))
    
        elif obj.type == PLANE:
            for l in self.lights:
                N = obj.s
                N.norm()#normal unit vector for plane

                L = Vektor(l.pos.x - p.x, l.pos.y - p.y, l.pos.z - p.z) #vector to light
                L.norm()
                
                fac = N.dotpr(L)
                
                
                #distance to light
                dtl = p.distance(l.pos)
                dtl /= 500.0
                fac = fac * 70.0/(dtl*dtl)
                

                if fac < 0:
                    fac = 0
                
                
                c = obj.mat.col
                dc = Vektor(KD*fac*c.x, KD*fac*c.y, KD*fac*c.z)
                dc = dc.scapr(l.pow) #lamp power
                if self.shadows(p,l, obj):
                    dc = dc.scapr(SHADOWCOEF)
                    
                    
                sumvek = sumvek.plus(dc)
                    
                sumvek = sumvek.plus(Vektor(KA*c.x, KA*c.y, KA*c.z))
                
    
    
        if obj.mat.ref > 0:
            sumvek = sumvek.scapr(1-obj.mat.ref)
            sumvek = sumvek.plus(self.reflect(p, obj, cam).scapr(obj.mat.ref))
    
        #check the range
        if sumvek.x > 255:
            sumvek.x = 255
        if sumvek.y > 255:
            sumvek.y = 255
        if sumvek.z > 255:
            sumvek.z = 255
        if sumvek.x < 0:
            sumvek.x = 0
        if sumvek.y < 0:
            sumvek.y = 0
        if sumvek.z < 0:
            sumvek.z = 0
            
        return sumvek



#SHADOWS---SHADOWS---SHADOWS---SHADOWS
    def shadows(self, p, l, sobj):
        vec = l.pos.minus(p)
        vec.norm()
        
        dist = l.pos.distance(p)#for some use
    
        for obj in self.thelist:
            if obj != sobj:
                if obj.type == BALL and obj.mat.lig != 1:
                    a = vec.x*vec.x + vec.y*vec.y + vec.z*vec.z
                    b = 2*vec.x*(p.x-obj.pos.x) +  2*vec.y*(p.y-obj.pos.y) +  2*vec.z*(p.z-obj.pos.z)
                    c = obj.pos.x*obj.pos.x + obj.pos.y*obj.pos.y + obj.pos.z*obj.pos.z + p.x*p.x + p.y*p.y + p.z*p.z - 2*(obj.pos.x*p.x + obj.pos.y*p.y + obj.pos.z*p.z) - obj.r*obj.r
            
                    disc = b*b - 4*a*c
                    if disc >= 0:
                        #if negative no intersection
                        t = -b - sqrt(disc) / (2*a)
                        if t > 0:
                            #if negative, the object is in the wrong direction
                            return True
        
        
                elif obj.type == PLANE:#might have problem with planes defined by reverse normal vector
                    return False#may be temporary but planes dont usually need shadows since they are infinite
                    '''
                    abc = obj.s
                    abc.norm()
                    d = obj.pos.distance(p)
                
                    disc = abc.dotpr(vec)
                    if disc != 0:
                        #if 0 plane and ray same direction -> no intersection
                        t = -(abc.dotpr(p)+d) / disc
                        if t > 0 and t <= d:
                            #if negative, the object is in the wrong direction
                            #ensures no shadow tracing after lamp
                            
                            return True
                    '''


        return False
    

#REFLECT---REFLECT---REFLECT---REFLECT

    def reflect(self, p, obj, cam):
        cvec = p.minus(cam)
        cvec.norm()
        
    
        if obj.type == BALL:
            N = Vektor((p.x - obj.pos.x)/obj.r, (p.y - obj.pos.y)/obj.r, (p.z - obj.pos.z)/obj.r) #normal unit vector for a sphere
        
        
    
        elif obj.type == PLANE:
            N = obj.s
            N.norm()#normal unit vector for plane
        
        
        
        

        refvec = cvec.minus(N.scapr(2 * (cvec.dotpr(N))))
        
        refvec.norm()
        
        #if obj.type == BALL:
            #refvec.z = refvec.z * -1 #tempfix
        
        '''
        if obj.type == BALL:
            print("N")
            print(N)
            print("cvec")
            print(cvec)
            print("refvec")
            print(refvec)
            raw_input("enter\n")
        '''
        
        
        '''
        if obj.type == PLANE:
            print (p)
            print (cvec)
            print(refvec)
            print ("--------------------------------------------------")
        '''
        return self.intersect(refvec, p, obj)#.scapr(obj.mat.ref)
        


	#uses pygame to draw the imagefile
    def drawimage(self):
        srf = pygame.Surface((SIZE,SIZE))
        pxar = pygame.PixelArray(srf)
        
        for i in range(SIZE):
            for j in range(SIZE):
                a = self.picture[i][j]
                
                
                pxar[i][j] = (a.x,a.y,a.z)

        srf = pxar.make_surface()
        pygame.image.save(srf, 'kuva.png')












