/*
 * plane.cc
 * c++ course project
 *
 */

#include "plane.h"

Plane::Plane(size_t id, int type, int color, size_t sprite, CL_Pointf pos): GameObject(id, type, pos) 
	{
		v.x = 0.0; 
		v.y = 0.0; 
		engine = false; 
		up = false; 
		down = false; 
		gun = false; 
		upsidedown = false; 
		bomb = false;
		pelican = false;
		faction = color;
		gravity = 0;
		lift = 0;
		airResistance = 0;
		acceleration = 0;
		velocity = 0;
		//timers
		atime = CL_System::get_time();
		btime = CL_System::get_time();
		ctime = CL_System::get_time();
		//plane status
		is_active = false;
		control = true;
		landed = false;
		alive = true;
		startgame = true;
		
		hp = HP;
		fuel = FUEL;
		ammo = AMMO;
		bombs = BOMBS;
		pelicans = PELICANS;
		
		angle = CL_Angle(0.0, cl_degrees);
		sprite_id = sprite;
		//creates collision outline
		outline = new CL_CollisionOutline("graphics/plane.png");
		outline->set_alignment(origin_center);
		outline->set_translation(pos.x, pos.y);
	}
	
	void Plane::update() {
		//temporary fixes for possible startup errors
		if (fabs(v.x) > 1000 || fabs(v.y) > 1000) {
			v.x = 0; v.y = 0;
			std::cout<<"speed error"<< std::endl;
		}
		if (pos.x>2*MAXX || pos.x< -(MAXX) || pos.y>2*MINY || pos.x< -(MINY)) {
			pos.x = base->getPos().x+50;
			pos.y = base->getPos().y-15;
			outline->set_translation(pos.x, pos.y);
			std::cout<<"position error"<< std::endl;
		}
		if (startgame) {
			v.y = 0;
			v.x = 0;
		}
		if(startgame && CL_System::get_time() > atime + 300) {
			startgame = false;
			is_active = true;
		}
		
		
		//moves the plane
		pos.x += v.x * 0.001*dt;
		pos.y += v.y * 0.001*dt;	
		
		//calculate mass, gravity and velocity
		mass = PLANE_MASS + fuel + ammo*0.1 + bombs*10;
		gravity = mass * GRAVITY;
		velocity = sqrt(v.x*v.x + v.y*v.y);
		
		//calculates difference between plane angle and actual direction
		CL_Angle difference;
		CL_Angle velAngle = angle;
		if (v.x != 0 && velocity > 3) {
			if (v.x > 0) {
				velAngle = CL_Angle(atan(v.y/v.x), cl_radians);
			}
			else {
				velAngle = CL_Angle(-180, cl_degrees) + CL_Angle(atan(v.y/v.x), cl_radians);
			}
		}
		
		difference = velAngle - angle;
		difference = difference.normalize_180();
		
		//calculates initial turn
		float init_turn = TURN / mass * sqrt(sqrt(velocity/1.5));
		if (velocity < 20) {
			init_turn = 0;
		}
		else if (velocity < 45) {
			init_turn *= 0.3;
		}
		
		double tvalue = 2/(1+(velocity/200));
		//plane controls
		if (up && control) {
			if (!upsidedown) {
				angle += CL_Angle(init_turn, cl_degrees);
				velAngle += CL_Angle(init_turn/ std::max(1.2, tvalue), cl_degrees);
			}
			else {
				angle -= CL_Angle(init_turn, cl_degrees);
				velAngle -= CL_Angle(init_turn/ std::max(1.2, tvalue), cl_degrees);
			}
			if (velocity > 50) {
				velocity -= 0.02;
			}
		}
		if (down && control) {
			if (!upsidedown) {
				angle -= CL_Angle(init_turn, cl_degrees);
				velAngle -= CL_Angle(init_turn/ std::max(1.2, tvalue), cl_degrees);
			}
			else {
				angle += CL_Angle(init_turn, cl_degrees);
				velAngle += CL_Angle(init_turn/ std::max(1.2, tvalue), cl_degrees);
			}
			if (velocity > 50) {
				velocity -= 0.02;
			}
		}
		
		v.x = cos(velAngle.to_radians())*velocity;
		v.y = sin(velAngle.to_radians())*velocity;
		
		if ((!control || velocity < 120) && velocity > 1 && !landed) {
			if(angle.normalize().to_degrees() < 90 || angle.normalize().to_degrees() > 270)
			{
				if (velAngle.normalize_180().to_degrees() > angle.normalize_180().to_degrees()) {
					angle += CL_Angle(0.2, cl_degrees);
				}
				else {
					angle -= CL_Angle(0.2, cl_degrees);
				}
			}
			else {
				if (velAngle.normalize() > angle.normalize()) {
					angle += CL_Angle(0.2, cl_degrees);
				}
				else {
					angle -= CL_Angle(0.2, cl_degrees);
				}
			}
		}
		
		if (engine && control && fuel > 0) {
			y_force += ENGINE_POWER * sin(angle.to_radians());
			x_force += ENGINE_POWER * cos(angle.to_radians());
			float fdt = dt;
			fuel -= fdt/1000;
		}
		//gravity
		y_force += gravity;
		//lift 
		if (angle.normalize().to_degrees() < 90 || angle.normalize().to_degrees() > 270) {
			if (difference.to_degrees() > 0) {
			lift = pow(velocity, 2) * LIFT_CL*(1+fabs(difference.to_radians()));
			}
			else {
				lift = pow(velocity, 2) * LIFT_CL* -(fabs(difference.to_radians()));
			}
		}
		else {
			if (difference.to_degrees() > 0) {
				lift = pow(velocity, 2) * LIFT_CL*-(fabs(difference.to_radians()));
			}
			else {
				lift = pow(velocity, 2) * LIFT_CL*(1+fabs(difference.to_radians()));
			} 
		}
		
		if (engine && control) {
			lift *= 1.5;
		}
		if ((up || down) && control) {
			lift *= 1.5;
		}
		if (!control) {
			lift *= 0.1;
		}
		
		//lift direction
		float liftx, lifty;
		if (v.x > 0 && !upsidedown) {
			lifty = -1*cos(angle.to_radians()) * lift;
			if (angle.normalize_180() >= CL_Angle(0, cl_degrees)) {
				liftx = sin(angle.to_radians()) * lift;
			}
			else {
				liftx = -1*sin(angle.to_radians()) * lift;
			}
		}
		else if (v.x < 0 && upsidedown) {
			lifty = cos(angle.to_radians()) * lift;
			if (angle.normalize_180() >= CL_Angle(0, cl_degrees)) {
				liftx = sin(angle.to_radians()) * lift;
			}
			else {
				liftx= -1*sin(angle.to_radians()) * lift;
			}
		}
		else if (v.x > 0 && upsidedown){
			lifty = 0.2* cos(angle.to_radians()) * lift;
			if (angle.normalize_180() >= CL_Angle(0, cl_degrees)) {
				liftx = sin(angle.to_radians()) * lift;
			}
			else {
				liftx = -1*sin(angle.to_radians()) * lift;
			}
		}
		else {
			lifty = 0.2* cos(angle.to_radians()) * lift;
			if (angle.normalize_180() >= CL_Angle(0, cl_degrees)) {
				liftx = -1*sin(angle.to_radians()) * lift;
			}
			else {
				liftx = sin(angle.to_radians()) * lift;
			}
		}
		x_force += liftx;
		y_force += lifty;
		
		//air_resistance
		airResistance = pow(velocity, 2) * AIR_CL * (1+2*pow(fabs(difference.to_radians()), 2));
		if (!control) {
			airResistance *= 0.1;
		}
		
		float xres, yres;
		CL_Angle resAngle = velAngle - CL_Angle(180, cl_degrees);
		xres = cos(resAngle.normalize().to_radians()) * airResistance;
		yres = sin(resAngle.normalize().to_radians()) * airResistance;
		
		x_force += xres;
		y_force += yres;
		

		//flipping the plane
		flipPlane();
		
		v.x += (x_force/mass) * 0.001*dt;
		v.y += (y_force/mass) * 0.001*dt;
		
		/*
		//gebug print prints all plane information
		if (faction ==1) {
			std::cout<<"posx: "<<posx<<"\t posy: "<<posy<<std::endl;
			std::cout<<"lift y: "<<lifty<<std::endl;
			std::cout<<"angle: "<<angle.to_degrees()<<"\t velAngle: "<<velAngle.to_degrees()<<" difference: "<<difference.to_degrees()<<std::endl;
			std::cout<<"v.x,v.y: "<<v.x<<" , "<<v.y<<"\t x_force,y_force: "<<x_force<< " , "<<y_force<<std::endl;
			std::cout<<"air resistance x: "<<xres<<std::endl;
			std::cout<<"air resistance y: "<<yres<<std::endl;
			std::cout<<"velocity : "<<velocity<<std::endl;
			std::cout<<"gravity: "<<gravity<<std::endl;
			std::cout<<"lift: "<<lift<<std::endl;
			std::cout<<"lift x: "<<liftx<<"\t lift y: "<<lifty<<std::endl;
			std::cout<<"fuel: "<<fuel<<std::endl;
			std::cout<<"HP: "<<hp<<std::endl;
		}
		*/
		
		y_force = 0;
		x_force = 0;
		
		//screen limits prevent exiting screen area
		if (pos.x <= MINX) {v.x = 100;}
		if (pos.x >= MAXX) {v.x = -100;}
		if (pos.y <= MINY) {hp = 0; v.y = 5;}
		if (pos.y >= MAXY) {hp -= 200;}
		
		if(gun && ammo > 0) {
			fireBullet();
		}
		
		if(bomb && bombs > 0) {
			dropBomb();
		}
		
		if(pelican && pelicans > 0) {
			dropPelican();
		}
		
		//moves the outline
		outline->set_translation(pos.x, pos.y);
		outline->set_angle(angle);
		
		//hp under 0 -> destruction
		if (hp <= 0) {
			destruction();
		}
		
		landed = false;
		//update the status object
		Stats::instance()->setAllStats(faction, bombs, fuel, ammo, pelicans);
	}
	
	
	//function destroys the plane
	void Plane::destruction() {
		if (hp <= 0 && is_active) {
			control = false;
			if (faction == 1) {sprite_id = FLIPANIMATION1;}
			else {sprite_id = FLIPANIMATION2;}
		}
		//destruction explosion
		if (hp < -50) {
			if (is_active) {
				is_active = false;
				control = false;
				sprite_id = GraphicEngine::instance()->createSprite(this, EXPLOSION2);
				ctime = CL_System::get_time();
			}
			if (GraphicEngine::instance()->getSprite(this)->is_finished()) {
				pos.x = base->getPos().x+45;
				pos.y = base->getPos().y-11;
				angle = CL_Angle(0, cl_degrees);
				GraphicEngine::instance()->removeSprite(this);
				upsidedown = false;
				if (faction == 1) {sprite_id = PLANE1_SPRITE;}
				else {sprite_id = PLANE2_SPRITE;}
				is_active = true;
				control = true;
				Stats::instance()->addKill(faction);
				refill();
				v.x = 0;
				v.y = 0;
				outline->set_translation(pos.x, pos.y);
				outline->set_angle(angle);
			}
		}
	}
	
	//flipping the plane
	void Plane::flipPlane() {
		if ((sprite_id == FLIPANIMATION1 || sprite_id == FLIPANIMATION2) && control) {
			if (CL_System::get_time() > ctime + 400) {
				GraphicEngine::instance()->getSprite(this)->restart();
				if (upsidedown) {
					if (faction == 1) {sprite_id = PLANE1FLIP_SPRITE;}
					else {sprite_id = PLANE2FLIP_SPRITE;}
				}
				if (!upsidedown) {
					if (faction == 1) {sprite_id = PLANE1_SPRITE;}
					else {sprite_id = PLANE2_SPRITE;}
				}
			}
		}
		if ((sprite_id == FLIPANIMATIONBACK1 || sprite_id == FLIPANIMATIONBACK2) && control) {
			if (CL_System::get_time() > ctime + 400) {
				GraphicEngine::instance()->getSprite(this)->restart();
				if (upsidedown) {
					if (faction == 1) {sprite_id = PLANE1FLIP_SPRITE;}
					else {sprite_id = PLANE2FLIP_SPRITE;}
				}
				if (!upsidedown) {
					if (faction == 1) {sprite_id = PLANE1_SPRITE;}
					else {sprite_id = PLANE2_SPRITE;}

				}
			}
		}
	}
	
	void Plane::doCollide(GameObject* obj) {
		switch (obj->getType()) {
			case TERRAIN: 
			case BARRACKS: {
				if (outline->collide(obj->getOutline())) {
					hp -= 200;
					v.x = v.x * 0.2;
					v.y = 0;
				}
				break;
			}
			case PLANE: {
				if (outline->collide(obj->getOutline())) {
				//atm planes do not collide
				}
				break;
			}
			case BULLET: {
				if (outline->point_inside(CL_Pointf(obj->getPos().x, obj->getPos().y))) {
					hp -= 7; //small damage
				}
				break;
			}
			case BOMB: 
			case AABULLET: {
				if (outline->point_inside(obj->getPos())) {
					hp -= 200;//instant destruction
				}
				else if (obj->getDestroying()) {
					float distance = sqrt(pow(pos.x-obj->getPos().x, 2) + pow(pos.y-obj->getPos().y, 2));
					if (distance < 15) {
						hp -= 60;
					}
					else if (distance < 30) {
						hp -= 50;
					}
					else if (distance < 40) {
						hp -= 30;
					}
					else if (distance < 55) {
						hp -= 10;
					}
				}
				break;
			}
			case PELICAN: {
				if (outline->collide(obj->getOutline())) {
					hp -= 90;//large damage
				}
				break;
			}
			case AIRFIELD: {//landing things
				if (outline->collide(obj->getOutline())) {
					if (!control || v.y > 40 || (angle.normalize_180().to_degrees() > 30 && angle.normalize_180().to_degrees() < 150) 
												|| (angle.normalize_180().to_degrees() < -40 && angle.normalize_180().to_degrees() > -140)) {
						v.y = 0;
						hp -= 200;
					}
					else {
						v.y = 0;
						if (!engine) {
							if (v.x > 0) {
								x_force = -33000;
							}
							else if (v.x < 0) {
								x_force = 33000;
							}
							
						}
						if (v.x < 0.5 && v.x > -0.5) {v.x = 0; landed = true;}
					}
				}
				break;
			}
			default: {
				break;
			}
		}
	}
	
	void Plane::fireBullet()
	{
		if (CL_System::get_time() > atime+100 && is_active) {
			Bullet* bullet = new Bullet(GameObject::getNextID(),BULLET, angle, velocity, this, pos);
			ObjectManager::instance()->registerObject(bullet);
			sound_id = SoundEngine::instance()->playSound(this, "SingleRiffleFire", false);
			atime = CL_System::get_time();
			ammo--;
		}
	}
	void Plane::dropBomb()
	{
		if (CL_System::get_time() > btime+500 && is_active) {
			Bomb* bomb = new Bomb(GameObject::getNextID(),BOMB, angle, v, this, pos);
			ObjectManager::instance()->registerObject(bomb);
			btime = CL_System::get_time();
			bombs--;
		}
	}
	void Plane::dropPelican() 
	{
		if (CL_System::get_time() > btime+500 && is_active) {
			Pelican* pelican = new Pelican(GameObject::getNextID(),PELICAN, v, this, pos);
			ObjectManager::instance()->registerObject(pelican);	
			btime = CL_System::get_time();
			pelicans--;
		}
	}
	
	void Plane::refill() {
		hp = HP;
		fuel = FUEL;
		ammo = AMMO;
		bombs = BOMBS;
		pelicans = PELICANS;
	}
	
