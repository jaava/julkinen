/*	Start menu class. 
*	Starts the game or optionsmenu. Exits the game if Exit is pressed. 
*	Creator: Juha Vanhalakka
*/
//#include "precomp.h"
#include "defines.h"
#include "graphicengine.h"
#include "world.h"
#include "objectmanager.h"
#include <iostream>
#include "optionsmenu.h"
#include "soundengine.h"


class StartMenu : public CL_GUIComponent
{
public:
	
	/* Constructor takes 
	*
	*/
	StartMenu(CL_GUIManager* man, CL_Rect pos, CL_DisplayWindow& window, CL_ResourceManager& resources);
	
	/* Adds MenuItems to vector. Takes CL_Sprites as parameters for different highlight states.
	*
	*/
	void addItem(CL_Sprite image_norm, CL_Sprite image_mouse, CL_Sprite image_clicked);
	
	/*	Initializes input for menu and calls exec(), which starts the menu.
	*
	*/
	void run();
	
		
private:
	
	
	/*	Struct to create custom GUI component. Constructor takes CL_Sprites as parameters
	*	and stores them in variables. 3 Sprites for dirrerent highlight states
	*/
	struct MenuItem
	{
		MenuItem(CL_Sprite &image_norm, CL_Sprite &image_mouse, CL_Sprite &image_clicked) : image_norm(image_norm), 
			image_mouse(image_mouse), image_clicked(image_clicked) {}
		CL_Sprite image_norm;
		CL_Sprite image_mouse;
		CL_Sprite image_clicked;
	};
	
	/*	Reads start_x, start_y and y_offset parameters from startmenu.css file. 
	*
	*/
	void create_menu_parts();
	
	/*	Function to handle MenuItem drawing.
	*
	*/
	void on_render(CL_GraphicContext &gc, const CL_Rect &update_rect);
	
	/* 	When message is send to StartMenu -class, this private function is called. Used to sort 
	*	messages.
	*	Parameters: CL_GUIMessage which hold information about pressed keys and mouse movements.
	*/	
	void on_process_message(CL_GUIMessage &msg);
	
	/* 	When mouse moves, this private function is called. Fuction checks what ever the mouse 
	*	cursor is inside the MenuItems rectangular. If so fuction changes 
	*	mouse_hover_index -variable.
	*	Parameters: CL_InputEvent, which holds information about the position of mouse cursor
	*/
	void mouse_moving(CL_InputEvent &input_event);
	
	/*	When left mouse button is pressed this fuction is called. Checks changes pressed_index to
	* 	correspond mouse_hover_index. 
	*	Paremeter: CL_InputEvent is not used.
	*/
	void left_button_down(CL_InputEvent &input_event);
	
	/* 	When mouse left button if released, this fuction is called. If pressed_index is something
	*	else than -1, fuction calls correspond event to initialize and start the game or OptionsMenu 
	*	Parameters: CL_InputEvent is not used in this case.
	*/
	void left_button_up(CL_InputEvent &input_event);
	
	/*	When Escape or exit button is pressed this fuction is called. Exits the component.
	*
	*/
	void on_window_close() {exit_with_code(1);}
	
	/*
	*	Variables to strore starting point of MenuItem components. Offset determinates 
	*	the y-difference between components.
	*/
	int start_x;
	int start_y;
	int offset_y;

	/*	Variables to store in set highlight states of MenuItem components.
	*/
	int pressed_index;
	int mouse_hover_index;
	

	CL_GUIManager* man;
	
	/*	Holds information readed from css file. 	
	*
	*/
	CL_GUIThemePart part_component;
	
	bool quit;
	
	/*	World needs these.
	*
	*/
	CL_DisplayWindow window;
	CL_ResourceManager resources;
	
	/*	Container to hold MenuItems.
	*/
	std::vector<MenuItem> menu_items;

};
