/*
 *	c++ course project triplane2
 *	graphicengine.cc
 *  Created by Jaakko Vallinoja
 */


#include "graphicengine.h"

GraphicEngine GraphicEngine::ge;

//draws gameresult when quiting
void GraphicEngine::drawResult() {
	CL_Font font = CL_Font(gc, "tahoma", 52);
	CL_String text;
	if (Stats::instance()->getKills(1) > Stats::instance()->getKills(2)) {
		text = CL_String("Player 1 wins!");
		font.draw_text(gc, 6*WIDHT/16, 1*HEIGHT/2, text);
	}
	else if (Stats::instance()->getKills(1) < Stats::instance()->getKills(2)) {
		text = CL_String("Player 2 wins!");
		font.draw_text(gc, 6*WIDHT/16, 1*HEIGHT/2, text);
	}
	else {
		text = CL_String("Draw");
		font.draw_text(gc, 7*WIDHT/16, 1*HEIGHT/2, text);
	}
	window.flip();
}

//draws information to the right side of the screen
void GraphicEngine::drawStats() {
		int a;
		//shows pl1 ammo
		CL_Draw::fill(gc, CL_Pointf(13*WIDHT/16, 2*HEIGHT/32), CL_Pointf(15*WIDHT/16, 3*HEIGHT/32), CL_Colorf::lightgrey);
		a = Stats::instance()->getAmmo(1) *(15*WIDHT/16 - 13*WIDHT/16)/AMMO;
		CL_Draw::fill(gc, CL_Pointf(13*WIDHT/16 + a, 2*HEIGHT/32), CL_Pointf(15*WIDHT/16, 3*HEIGHT/32), CL_Colorf::darkgrey);
		//pl1 bombs
		int b = 0;
		size_t i = 0;
		while (i < 10) {
			sprites[BOMB_SPRITE]->draw(gc, 13*WIDHT/16 +5+ b, 4*HEIGHT/32);
			b += 13; i++;
		}
		b = 117; i = 0;
		while (i < 10 - Stats::instance()->getBombs(1)) {
			sprites[BOMB_SPRITE2]->draw(gc, 13*WIDHT/16 +5+ b, 4*HEIGHT/32);
			b -= 13; i++;
		}
		
		//pl1 pelicans
		b = 0;
		i = 0;
		while (i < 5) {
			sprites[PELICAN_SPRITE]->draw(gc, 13*WIDHT/16 + b, 13*HEIGHT/64);
			b += 25; i++;
		}
		b = 100; i = 0;
		while (i < 5 - Stats::instance()->getPelicans(1)) {
			sprites[PELICAN_SPRITE2]->draw(gc, 13*WIDHT/16 +b, 13*HEIGHT/64);
			b -= 25; i++;
		}
		
		//pl1 fuel
		CL_Draw::fill(gc, CL_Pointf(13*WIDHT/16, 7*HEIGHT/32), CL_Pointf(15*WIDHT/16, 8*HEIGHT/32), CL_Colorf::red);
		a = Stats::instance()->getFuel(1) *(15*WIDHT/16 - 13*WIDHT/16)/FUEL;
		CL_Draw::fill(gc, CL_Pointf(13*WIDHT/16 + a, 7*HEIGHT/32), CL_Pointf(15*WIDHT/16, 8*HEIGHT/32), CL_Colorf::darkred);
		
		
		//shows pl2 ammo
		CL_Draw::fill(gc, CL_Pointf(13*WIDHT/16, 18*HEIGHT/32), CL_Pointf(15*WIDHT/16, 19*HEIGHT/32), CL_Colorf::lightgrey);
		a = Stats::instance()->getAmmo(2) *(15*WIDHT/16 - 13*WIDHT/16)/AMMO;
		CL_Draw::fill(gc, CL_Pointf(13*WIDHT/16 + a, 18*HEIGHT/32), CL_Pointf(15*WIDHT/16, 19*HEIGHT/32), CL_Colorf::darkgrey);
		//pl2 bombs
		b = 0;
		i = 0;
		while (i < 10) {
			sprites[BOMB_SPRITE]->draw(gc, 13*WIDHT/16 +5+ b, 20*HEIGHT/32);
			b += 13; i++;
		}
		b = 117; i = 0;
		while (i < 10 - Stats::instance()->getBombs(2)) {
			sprites[BOMB_SPRITE2]->draw(gc, 13*WIDHT/16 +5+ b, 20*HEIGHT/32);
			b -= 13; i++;
		}
		
		//pl2 pelicans
		b = 0;
		i = 0;
		while (i < 5) {
			sprites[PELICAN_SPRITE]->draw(gc, 13*WIDHT/16 + b, 22*HEIGHT/32);
			b += 25; i++;
		}
		b = 100; i = 0;
		while (i < 5 - Stats::instance()->getPelicans(2)) {
			sprites[PELICAN_SPRITE2]->draw(gc, 13*WIDHT/16 +b, 22*HEIGHT/32);
			b -= 25; i++;
		}
		
		//testdraw shows pl2 fuel
		CL_Draw::fill(gc, CL_Pointf(13*WIDHT/16, 23*HEIGHT/32), CL_Pointf(15*WIDHT/16, 24*HEIGHT/32), CL_Colorf::red);
		a = Stats::instance()->getFuel(2) *(15*WIDHT/16 - 13*WIDHT/16)/FUEL;
		CL_Draw::fill(gc, CL_Pointf(13*WIDHT/16 + a, 23*HEIGHT/32), CL_Pointf(15*WIDHT/16, 24*HEIGHT/32), CL_Colorf::darkred);
		
		//points
		CL_Font font = CL_Font(gc, "tahoma", 24);
		CL_String text("Score:");
		font.draw_text(gc, 13*WIDHT/16, 10*HEIGHT/32, text);
		text = CL_String(CL_StringHelp::int_to_text(Stats::instance()->getKills(1)));
		font.draw_text(gc, 14*WIDHT/16, 11*HEIGHT/32, text);
		text = CL_String("Score:");
		font.draw_text(gc, 13*WIDHT/16, 26*HEIGHT/32, text);
		text = CL_StringHelp::int_to_text(Stats::instance()->getKills(2));
		font.draw_text(gc, 14*WIDHT/16, 27*HEIGHT/32, text);
		
	}
	
//draws objects with doDraw //draws everything visible on the screen
void GraphicEngine::draw() {

	gc.clear(CL_Colorf::skyblue);
	
	//ask object manager for objects
	ObjectManager::instance()->draw();
	
	//CL_Draw::fill(gc, CL_Pointf(3*WIDHT/4, 0), CL_Pointf(WIDHT, HEIGHT), CL_Colorf::saddlebrown);//status screen backround
	sprites[GUIBACKROUND]->draw(gc, WIDHT, 0);
	sprites[GUIBACKROUND]->draw(gc, WIDHT, HEIGHT/2);
	CL_Draw::fill(gc, CL_Pointf(0, HEIGHT/2 - 3), CL_Pointf(WIDHT, HEIGHT/2 + 3), CL_Colorf::black);//horizontal line dividing the screen
	CL_Draw::fill(gc, CL_Pointf(3*WIDHT/4, 0), CL_Pointf(3*WIDHT/4 + 6, HEIGHT), CL_Colorf::black);//vertical line dividing the screen
	drawStats();
	
	//and make it visible
	window.flip();
}

void GraphicEngine::doDraw(GameObject* object)
{
	sprites[object->getSpriteId()]->update();
	//draws given object
	//ifs below implemet the scrolling screen and split screen functionality
	sprites[object->getSpriteId()]->set_angle(object->getAngle());
	//horizontal split		
		if (pl1->getPos().x < 3*WIDHT/8) {
			sprites[object->getSpriteId()]->draw(gc, object->getPos().x, object->getPos().y - HEIGHT/2);
		}
		else if (pl1->getPos().x > MAPWIDHT - 3*WIDHT/8) {
			sprites[object->getSpriteId()]->draw(gc, object->getPos().x - (MAPWIDHT - 3*WIDHT/4), object->getPos().y - HEIGHT/2);
		}
		else {
			sprites[object->getSpriteId()]->draw(gc, object->getPos().x - (pl1->getPos().x - 3*WIDHT/8), object->getPos().y - HEIGHT/2);
		}
		
		//pl2 point of wiev
		if (object->getPos().y > MINY) {//to prevent drawing to pl1 window 
			if (pl2->getPos().x < 3*WIDHT/8) {
				sprites[object->getSpriteId()]->draw(gc, object->getPos().x, object->getPos().y);
			}
			else if (pl2->getPos().x > MAPWIDHT - 3*WIDHT/8) {
				sprites[object->getSpriteId()]->draw(gc, object->getPos().x - (MAPWIDHT - 3*WIDHT/4), object->getPos().y);
			}
			else {
				sprites[object->getSpriteId()]->draw(gc, object->getPos().x - (pl2->getPos().x - 3*WIDHT/8), object->getPos().y);
			}
	}
}

void GraphicEngine::staticSprites() {
	CL_Sprite* plane1 = new CL_Sprite(gc, "Biplane", &resources);
	CL_Sprite* plane2 = new CL_Sprite(gc, "Biplane", &resources);
	plane2->set_color(CL_Colorf::red);
	sprites[PLANE1_SPRITE] = plane1;
	sprites[PLANE2_SPRITE] = plane2;
	CL_Sprite* plane1flip = new CL_Sprite(gc, "Biplaneflip", &resources);
	CL_Sprite* plane2flip = new CL_Sprite(gc, "Biplaneflip", &resources);
	plane2flip->set_color(CL_Colorf::red);
	sprites[PLANE1FLIP_SPRITE] = plane1flip;
	sprites[PLANE2FLIP_SPRITE] = plane2flip;
	
	CL_Sprite* flipanimation1 = new CL_Sprite(gc, "Biplane_flipping", &resources);
	sprites[FLIPANIMATION1] = flipanimation1;
	CL_Sprite* flipanimation2 = new CL_Sprite(gc, "Biplane_flipping", &resources);
	sprites[FLIPANIMATION2] = flipanimation2;
	flipanimation2->set_color(CL_Colorf::red);
	CL_Sprite* flipanimation1back = new CL_Sprite(gc, "Biplane_flipping2", &resources);
	sprites[FLIPANIMATIONBACK1] = flipanimation1back;
	CL_Sprite* flipanimation2back = new CL_Sprite(gc, "Biplane_flipping2", &resources);
	sprites[FLIPANIMATIONBACK2] = flipanimation2back;
	flipanimation2back->set_color(CL_Colorf::red);
	
	CL_Sprite* terrain = new CL_Sprite(gc, "Terrain", &resources);
	sprites[TERRAIN_SPRITE]= terrain;
	CL_Sprite* terrain2 = new CL_Sprite(gc, "Terrain2", &resources);
	sprites[TERRAIN_SPRITE2]= terrain2;
	CL_Sprite* bullet = new CL_Sprite(gc, "Bullet", &resources);
	sprites[BULLET_SPRITE]= bullet;
	CL_Sprite* bulletr = new CL_Sprite(gc, "Bullet", &resources);
	sprites[BULLET_SPRITE_RED]= bulletr;
	sprites[BULLET_SPRITE_RED]->set_scale(2,2);
	sprites[BULLET_SPRITE_RED]->set_color(CL_Colorf::red);
	CL_Sprite* bomb = new CL_Sprite(gc, "Bomb", &resources);
	CL_Sprite* bomb2 = new CL_Sprite(gc, "Bomb", &resources);
	sprites[BOMB_SPRITE]= bomb;
	sprites[BOMB_SPRITE2]= bomb2;
	sprites[BOMB_SPRITE]->set_scale(2.1,2.8);
	sprites[BOMB_SPRITE2]->set_scale(2.1,2.8);
	sprites[BOMB_SPRITE2]->set_color(CL_Colorf::red);
	
	CL_Sprite* airfield = new CL_Sprite(gc, "Base", &resources);
	sprites[AIRFIELD_SPRITE]= airfield;
	CL_Sprite* airfieldb = new CL_Sprite(gc, "Baseb", &resources);
	sprites[AIRFIELDBACK_SPRITE]= airfieldb;
	
	CL_Sprite* soldierleft = new CL_Sprite(gc, "Soldier_left", &resources);
	sprites[SOLDIERWALKINGLEFT]= soldierleft;
	CL_Sprite* soldierright = new CL_Sprite(gc, "Soldier_right", &resources);
	sprites[SOLDIERWALKINGRIGHT]= soldierright;
	CL_Sprite* soldierfirel = new CL_Sprite(gc, "Soldier_shoot_left", &resources);
	sprites[SOLDIERFIRINGLEFT]= soldierfirel;
	CL_Sprite* soldierfirer = new CL_Sprite(gc, "Soldier_shoot_right", &resources);
	sprites[SOLDIERFIRINGRIGHT]= soldierfirer;
	
	CL_Sprite* pelican = new CL_Sprite(gc, "Pelican_s", &resources);
	sprites[PELICAN_SPRITE]= pelican;
	CL_Sprite* pelican2 = new CL_Sprite(gc, "Pelican_s", &resources);
	sprites[PELICAN_SPRITE2]= pelican2;
	//sprites[PELICAN_SPRITE]->set_scale(0.7,0.7);
	//sprites[PELICAN_SPRITE2]->set_scale(0.7,0.7);
	sprites[PELICAN_SPRITE2]->set_color(CL_Colorf::darkgrey);
	
	CL_Sprite* barracks = new CL_Sprite(gc, "Barracks", &resources);
	sprites[BARRACKS_SPRITE]= barracks;
	CL_Sprite* barracksd = new CL_Sprite(gc, "BarracksD", &resources);
	sprites[BARRACKSD_SPRITE]= barracksd;
	
	CL_Sprite* gui_backround = new CL_Sprite(gc, "Gui_backround", &resources);
	sprites[GUIBACKROUND]= gui_backround;
	CL_Sprite* flag = new CL_Sprite(gc, "Flag", &resources);
	sprites[FLAG_SPRITE]= flag;
	CL_Sprite* flag2 = new CL_Sprite(gc, "Flag", &resources);
	sprites[FLAG2_SPRITE]= flag2;
	sprites[FLAG2_SPRITE]->set_color(CL_Colorf::red);
	
	CL_Sprite* aabase = new CL_Sprite(gc, "AaBase", &resources);
	sprites[AABASE_SPRITE]= aabase;
	CL_Sprite* aabased = new CL_Sprite(gc, "AaBaseD", &resources);
	sprites[AABASEDESTROY_SPRITE]= aabased;
	
	CL_Sprite* palm1 = new CL_Sprite(gc, "Palm1", &resources);
	sprites[PALM1]= palm1;
	CL_Sprite* palm2 = new CL_Sprite(gc, "Palm2", &resources);
	sprites[PALM2]= palm2;
	CL_Sprite* cloud1 = new CL_Sprite(gc, "Cloud1", &resources);
	sprites[CLOUD1]= cloud1;
	CL_Sprite* cloud2 = new CL_Sprite(gc, "Cloud2", &resources);
	sprites[CLOUD2]= cloud2;
	
}

	
//setter for window and gc
void GraphicEngine::setgraphics(CL_GraphicContext _gc, CL_DisplayWindow _window) {
	gc = _gc;
	window = _window;
}
//setter for resources
void GraphicEngine::setResources(const CL_ResourceManager& _man) {
	resources = _man;
}
//tells graphing engine, which objects are the players
void GraphicEngine::setPlayers(GameObject* p1, GameObject* p2) {
	pl1 = p1;
	pl2 = p2;
}
//remove the spritemap
void GraphicEngine::removeAllSprites() {
  	std::map<size_t, CL_Sprite*>::iterator it = sprites.begin();
  	while (it != sprites.end()){
    	delete it->second;
    	it++;
  	}
  	sprites.erase(sprites.begin(), it);
}

//creates a new sprite
size_t GraphicEngine::createSprite(GameObject* object, int sprite_type) {
	size_t id = 0;
	switch (sprite_type) {
		case BOMB_SPRITE: {
			CL_Sprite* bomb = new CL_Sprite(gc, "Bomb", &resources);
			id = 40; //leaves room for static sprites
			while (true) {
				if (!sprites.count(id)) {
					sprites[id] = bomb;
					break;
				}
				id++;
			}
			break;
		}
		case EXPLOSION2: {
			CL_Sprite* explosion = new CL_Sprite(gc, "Explosion2", &resources);
			id = 40; //leaves room for static sprites
			while (true) {
				if (!sprites.count(id)) {
					sprites[id] = explosion;
					break;
				}
				id++;
			}
			break;
		}
		case EXPLOSION1: {
			CL_Sprite* explosion = new CL_Sprite(gc, "Explosion1", &resources);
			id = 40; //leaves room for static sprites
			while (true) {
				if (!sprites.count(id)) {
					sprites[id] = explosion;
					break;
				}
				id++;
			}
			break;
		}
		case SOLDIERWALKINGLEFT: {
			CL_Sprite* soldier = new CL_Sprite(gc, "Soldier_left", &resources);
			id = 40; //leaves room for static sprites
			while (true) {
				if (!sprites.count(id)) {
					sprites[id] = soldier;
					break;
				}
				id++;
			}
			break;
		}
		case SOLDIERWALKINGRIGHT: {
			CL_Sprite* soldier = new CL_Sprite(gc, "Soldier_right", &resources);
			id = 40; //leaves room for static sprites
			while (true) {
				if (!sprites.count(id)) {
					sprites[id] = soldier;
					break;
				}
				id++;
			}
			break;
		}
		case SOLDIERFIRINGLEFT: {
			CL_Sprite* soldier = new CL_Sprite(gc, "Soldier_shoot_left", &resources);
			id = 40; //leaves room for static sprites
			while (true) {
				if (!sprites.count(id)) {
					sprites[id] = soldier;
					break;
				}
				id++;
			}
			break;
		}
		case SOLDIERFIRINGRIGHT: {
			CL_Sprite* soldier = new CL_Sprite(gc, "Soldier_shoot_right", &resources);
			id = 40; //leaves room for static sprites
			while (true) {
				if (!sprites.count(id)) {
					sprites[id] = soldier;
					break;
				}
				id++;
			}
			break;
		}
		case PELICANLEFT: {
			CL_Sprite* pelican = new CL_Sprite(gc, "Pelicanl", &resources);
			id = 40; //leaves room for static sprites
			while (true) {
				if (!sprites.count(id)) {
					sprites[id] = pelican;
					break;
				}
				id++;
			}
			break;
		}
		case PELICANRIGHT: {
			CL_Sprite* pelican = new CL_Sprite(gc, "Pelicanr", &resources);
			id = 40; //leaves room for static sprites
			while (true) {
				if (!sprites.count(id)) {
					sprites[id] = pelican;
					break;
				}
				id++;
			}
			break;
		}
		case SOLDIERDIELEFT: {
			CL_Sprite* soldier = new CL_Sprite(gc, "SoldierDieL", &resources);
			id = 40; //leaves room for static sprites
			while (true) {
				if (!sprites.count(id)) {
					sprites[id] = soldier;
					break;
				}
				id++;
			}
			break;
		}
		case SOLDIERDIERIGHT: {
			CL_Sprite* soldier = new CL_Sprite(gc, "SoldierDieR", &resources);
			id = 40; //leaves room for static sprites
			while (true) {
				if (!sprites.count(id)) {
					sprites[id] = soldier;
					break;
				}
				id++;
			}
			break;
		}
		case PELICANDIEL: {
			CL_Sprite* pelican = new CL_Sprite(gc, "PelicanDiel", &resources);
			id = 40; //leaves room for static sprites
			while (true) {
				if (!sprites.count(id)) {
					sprites[id] = pelican;
					break;
				}
				id++;
			}
			break;
		}
		case PELICANDIER: {
			CL_Sprite* pelican = new CL_Sprite(gc, "PelicanDier", &resources);
			id = 40; //leaves room for static sprites
			while (true) {
				if (!sprites.count(id)) {
					sprites[id] = pelican;
					break;
				}
				id++;
			}
			break;
		}
		case AAGUN_SPRITE: {
			CL_Sprite* aagun = new CL_Sprite(gc, "AABarrelL", &resources);
			id = 40; //leaves room for static sprites
			while (true) {
				if (!sprites.count(id)) {
					sprites[id] = aagun;
					break;
				}
				id++;
			}
			break;
		}
		case AAGUNFLIP_SPRITE: {
			CL_Sprite* aagun = new CL_Sprite(gc, "AABarrelR", &resources);
			id = 40; //leaves room for static sprites
			while (true) {
				if (!sprites.count(id)) {
					sprites[id] = aagun;
					break;
				}
				id++;
			}
			break;
		}
		case AAGUNFIRE: {
			CL_Sprite* aagun = new CL_Sprite(gc, "AAFireLeft", &resources);
			id = 40; //leaves room for static sprites
			while (true) {
				if (!sprites.count(id)) {
					sprites[id] = aagun;
					break;
				}
				id++;
			}
			break;
		}
		case AAGUNFIREFLIP: {
			CL_Sprite* aagun = new CL_Sprite(gc, "AAFireRight", &resources);
			id = 40; //leaves room for static sprites
			while (true) {
				if (!sprites.count(id)) {
					sprites[id] = aagun;
					break;
				}
				id++;
			}
			break;
		}
		case AABASEDESTROY_SPRITE: {
			CL_Sprite* aabase = new CL_Sprite(gc, "AaBaseDA", &resources);
			id = 40; //leaves room for static sprites
			while (true) {
				if (!sprites.count(id)) {
					sprites[id] = aabase;
					break;
				}
				id++;
			}
			break;
		}
	}
	return id;
}

void GraphicEngine::removeSprite (GameObject* object) {
	delete sprites[object->getSpriteId()];
	sprites.erase(object->getSpriteId());
}

CL_Sprite* GraphicEngine::getSprite (GameObject* object) {
	return sprites[object->getSpriteId()];
}


