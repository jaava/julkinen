/*	Options menu class. 
*	Creates the options menu and its components. Calls Inputs and SoundEngines setter
*	and getter fuctions to configure buttons and sound volumes.
*
*	Creator: Juha Vanhalakka
*/


#ifndef CPP_OPTIONSMENU
#define CPP_OPTIONSMENU

//#include "precomp.h"
#include "defines.h"
#include "input.h"
#include "soundengine.h"
#include <vector>

class OptionsMenu : public CL_GUIComponent
{
public:

	/* Constuctor crates all the menu components. Stars the menu. 	
	*	Parameters: CL_GUIManager to get the information about the menu layout.
	*				pos is the positon of the screen. window is used to set the 
	*				toplevelcomponent, which is needed in order to show menu right.
	*/
	OptionsMenu(CL_GUIManager* man, CL_Rect pos, CL_DisplayWindow &window);

	

private:
	
	/*	Called when button is keyboard button is pressed. If Input::setPlayerKey return true, the key is succesfully 
	*	changed. Then function changes also the text of the corresponding CL_LineEdit component. If key is already in 
	*	use, function does nothing and sets value back.
	*	Parameters: CL_Input event to get correspond key. CL_InputState is not used.
	*/
	void on_input_down(const CL_InputEvent &key, const CL_InputState &state);
	
	/*	Called when CL_LineEdit is gains focus by mouse click.
	*	Parameter: not used.
	*/
	void on_line_edit_clicked(CL_LineEdit *line);

	/*	Called if minus button is pressed
	*	Parameter: corresponding CL_ProgressBar pointer to update its value
	*/
	void on_minus_button_clicked(CL_ProgressBar *bar);

	/*	Called if plus button of is pressed.
	*	Parameters: corresponding CL_ProgressBar pointer to set update its value
	*/
	void on_plus_button_clicked(CL_ProgressBar *bar);
	
	/*	Call when "Back" button is pressed. Exits the OptionsMenu.
	*
	*/	
	void on_back_clicked();
	
	/*	Does nothin usefull right now.
	*
	*/	
	bool on_page_focus();
	
	/*	Used to create CL_GUITopLevelDescription for the OptionsMenu.
	*	Parameters: CL_DisplayWindow reference which was created main.cc
	*/		
	CL_GUITopLevelDescription get_top_level_description(CL_DisplayWindow&);	
	

	/*	Uses to store pressed key name.	
	*
	*/
	CL_String pressed;
	
	//slot is used to capture key when its about to be changed
	CL_Slot slotti;
	
	/*	Container to hold CL_LineEdit pointers. CL_LineEdits are used to display player keys.
	*	
	*/
	std::vector<CL_LineEdit*>  editit;
	
};

#endif
