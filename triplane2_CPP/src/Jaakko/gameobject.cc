/*
 *  gameobject.cc
 *  c++ course project triplane2
 *
 *  Created by Jaakko Vallinoja on 11/10/11.
 *
 */

#include "gameobject.h"

//initialize the static variables id_count, dt and players
size_t GameObject::id_count = 0;
size_t GameObject::dt = 0;

void GameObject::setTime(size_t newdt) {
	dt = newdt;
}

void GameObject::reset_id_count() {
	id_count = 0;
}


