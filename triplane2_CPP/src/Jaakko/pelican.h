/*
 *  pelican.h
 *  c++ course project triplane2
 *  Created by Jaakko Vallinoja on 11/30/11.
 *
 */

#ifndef CPP_PROJECT_PELICAN
#define CPP_PROJECT_PELICAN

#include "gameobject.h"
#include "soundengine.h"
#include <cmath>

class Pelican : public GameObject
{
public:
	Pelican(size_t id, int type, CL_Pointf _v, GameObject* _owner, CL_Pointf _pos): GameObject(id, type, _pos), w(v)
	{
		posx1 = _pos.x;
		posy1 = _pos.y;
		if (_v.x > 0) {
			v.x = -80;
			sprite_id = GraphicEngine::instance()->createSprite(this, PELICANLEFT);
		}
		else {
			v.x = 80;
			sprite_id = GraphicEngine::instance()->createSprite(this, PELICANRIGHT);
		}
		is_active = false;
		atime = CL_System::get_time();
		is_destroying = false;
		alive = true;
		
		outline = new CL_CollisionOutline("graphics/PelicanRight/pelicanRight1.png");
		outline->set_alignment(origin_center);
		outline->set_translation(_pos.x, _pos.y);
		sound_id = SoundEngine::instance()->playSound(this, "Pelican", true);
	}
	
	// pelican moves following a sine function
	void update() {
		pos.x += v.x * 0.001*dt;
		pos.y = 70 * sin((pos.x-posx1)/50) + posy1;
		outline->set_translation(pos.x, pos.y);
		
		if (CL_System::get_time() > atime + 800 && !is_active) {
			is_active = true;
		}
		
		if (pos.x > MAXX + 1000 || pos.x < MINX -1000) {
			alive = false;
			GraphicEngine::instance()->removeSprite(this);
			SoundEngine::instance()->stopSound(sound_id);
			return;
		}
		
		if (is_destroying) {
			is_active = false;
			
			//add effects here
			if (CL_System::get_time() > atime +750) {
				alive = false;
				GraphicEngine::instance()->removeSprite(this);
				SoundEngine::instance()->stopSound(sound_id);
				return;
			}
		}
		
	}
	
	void doCollide(GameObject* obj) {
		switch (obj->getType()) {
			case TERRAIN: 
			case PLANE: 
			case PELICAN: {
				if (outline->collide(obj->getOutline())) {
					is_destroying = true;
					SoundEngine::instance()->stopSound(sound_id);
					GraphicEngine::instance()->removeSprite(this);
					if (sprite_id == PELICANLEFT) {
						sprite_id = GraphicEngine::instance()->createSprite(this, PELICANDIEL);
					}
					else {
						sprite_id = GraphicEngine::instance()->createSprite(this, PELICANDIER);
					}
					atime = CL_System::get_time();
					break;
				}
			}
			case BULLET: 
			case BOMB: {
				if (outline->point_inside(obj->getPos())) {
					is_destroying = true;
					SoundEngine::instance()->stopSound(sound_id);					
					GraphicEngine::instance()->removeSprite(this);
					if (sprite_id == PELICANLEFT) {
						sprite_id = GraphicEngine::instance()->createSprite(this, PELICANDIEL);
					}
					else {
						sprite_id = GraphicEngine::instance()->createSprite(this, PELICANDIER);
					}
					atime = CL_System::get_time();
					break;
				}
			}
			default: {break;}
		}
	}
	
	~Pelican() {delete outline;}
	
private:
	CL_Pointf v;
	CL_Pointf w;
	float posx1;
	float posy1;
	size_t atime;
	size_t sound_id;
	
};

#endif
