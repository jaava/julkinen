/*
 * c++ course project triplane2
 * objectmanager.hh
 * created by Jaakko Vallinoja
 *
 */
#ifndef CPP_PROJECT_OBJECTMANAGER
#define CPP_PROJECT_OBJECTMANAGER


#include <map>
#include "gameobject.h"
#include "graphicengine.h"

class GameObject; 

//object manager takes care of all the objects in the game
class ObjectManager
{
public:
	static ObjectManager* instance() {return &manager;}
	//add object
	void registerObject(GameObject* ptr);
 	//remove object
	void removeObject(GameObject * ptr);
 
 	//delete all objects
	void releaseObjects();
	
	void setResources(const CL_ResourceManager& parman) {resources = parman;}//this too might still be removed if not needed

 	//updates every object
	void update();
	
	//every object collision check
	void collide();
	
	//asks for every object for drawing
	void draw();

	GameObject* getObjectFromID(size_t id);
	
	

  //destructor uses release objects to make sure every object is destroyed
	~ObjectManager() {releaseObjects();}


private:
  //constructors
	ObjectManager() {}
	ObjectManager(const ObjectManager&);
	ObjectManager& operator=(const ObjectManager&);

  //container and the manager itself
	static ObjectManager manager;
	std::map<size_t, GameObject*> container;
	
	CL_ResourceManager resources;

};

#endif
