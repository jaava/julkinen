
//#include "precomp.h"
#include "menus.h"
#include <iostream>


StartMenu::StartMenu(CL_GUIManager* man, CL_Rect pos, CL_DisplayWindow& window, CL_ResourceManager& resources): CL_GUIComponent(man, CL_GUITopLevelDescription(pos, false)), window(window), resources(resources), pressed_index(-1), mouse_hover_index(-1), man(man)
{
    set_type_name("startmenu"); // for CL_GUITheme part to know, which file to look for.
	addItem(CL_Sprite(window.get_gc(), "graphics/Gui/GuiGraphics/StartGameNormal.png"), CL_Sprite(window.get_gc(), "graphics/Gui/GuiGraphics/StartGameMouse.png"), CL_Sprite(window.get_gc(), "graphics/Gui/GuiGraphics/StartGamePressed.png"));
	addItem(CL_Sprite(window.get_gc(), "graphics/Gui/GuiGraphics/OptionsNormal.png"), CL_Sprite(window.get_gc(), "graphics/Gui/GuiGraphics/OptionsMouse.png"), CL_Sprite(window.get_gc(), "graphics/Gui/GuiGraphics/OptionsPressed.png"));
	addItem(CL_Sprite(window.get_gc(), "graphics/Gui/GuiGraphics/ExitNormal.png"), CL_Sprite(window.get_gc(), "graphics/Gui/GuiGraphics/ExitMouse.png"), CL_Sprite(window.get_gc(), "graphics/Gui/GuiGraphics/ExitPressed.png"));    

	func_render().set(this, &StartMenu::on_render); //to set specified render fuction for graphics
    func_process_message().set(this, &StartMenu::on_process_message); //to set specified message handling
    create_menu_parts();    //creates menuparts.
    SoundEngine::instance()->setMusic("sounds/SoldiersAndPilgrims.ogg"); //start music.
	
    
}

void StartMenu::addItem(CL_Sprite image_norm, CL_Sprite image_mouse, CL_Sprite image_clicked)
{
	menu_items.push_back(MenuItem(image_norm, image_mouse, image_clicked));
    
}


void StartMenu::run()
{
    
    //to make program close if window is closed
    CL_Slot slot_quit = window.sig_window_close().connect(this, &StartMenu::on_window_close);
    
    CL_InputDevice mouse = window.get_ic().get_mouse();
    CL_InputDevice keyboard = window.get_ic().get_keyboard();
    cl_log_event("info", "startx: %1  starty: %2 offsety: %3", start_x, start_y, offset_y);
    
    exec();
    
    
}



void StartMenu::create_menu_parts()
{
	
    part_component = CL_GUIThemePart(this);     // creates part_component

    // reads properties for .css -file
	CL_GUIThemePartProperty prop_start_x("start-x");
	start_x = CL_StringHelp::text_to_int(part_component.get_property(prop_start_x));
	CL_GUIThemePartProperty prop_start_y("start-y");
	start_y = CL_StringHelp::text_to_int(part_component.get_property(prop_start_y));
	CL_GUIThemePartProperty prop_offset_y("offset-y");
	offset_y = CL_StringHelp::text_to_int(part_component.get_property(prop_offset_y));
}


void StartMenu::on_render(CL_GraphicContext &gc, const CL_Rect &update_rect)
{
	part_component.render_box(gc, get_geometry().get_size(), update_rect);

	int pos_x = start_x;
	int pos_y = start_y;
    
    
    //iterates the the vector of MenuItems and checks if mouse_hover_index or
    //mouse_pressed_index maches the menu index.
	for(unsigned int i = 0; i < menu_items.size(); ++i)
	{
		if(i == pressed_index)
			menu_items[i].image_clicked.draw(gc, (float)pos_x, (float)pos_y);
		else if(i == mouse_hover_index)
			menu_items[i].image_mouse.draw(gc, (float)pos_x, (float)pos_y);
		else
			menu_items[i].image_norm.draw(gc, (float)pos_x, (float)pos_y);

		pos_y += offset_y;
	}
}


void StartMenu::on_process_message(CL_GUIMessage &msg)
{
	if (msg.is_type(CL_GUIMessage_Input::get_type_name()) && is_enabled())
	{
		CL_GUIMessage_Input input = msg;
		CL_InputEvent input_event = input.get_event();
		if (input_event.type == CL_InputEvent::pointer_moved){
			mouse_moving(input_event);
			}
		
		else if (input_event.type == CL_InputEvent::pressed && input_event.id == CL_MOUSE_LEFT)
			left_button_down(input_event);
		else if (input_event.type == CL_InputEvent::released && input_event.id == CL_MOUSE_LEFT)
			left_button_up(input_event);
		
	}
}


void StartMenu::mouse_moving(CL_InputEvent &input_event)
{
	int pos_x = start_x;
	int pos_y = start_y;
	for(unsigned int i = 0; i < menu_items.size(); ++i)
	{
		
        // crates the area of MenuItem.
        CL_Rect icon_rect(pos_x, pos_y, pos_x + menu_items[i].image_norm.get_width(), pos_y + menu_items[i].image_norm.get_height());
        
        //checks if the mouse cursor is inside icon_rect
		if(icon_rect.contains(input_event.mouse_pos))
		{
			mouse_hover_index = i;
			request_repaint();
			return;
		}

		pos_y += offset_y;
	}

    //if mouse is not inside any component, highlight indexes are set back to -1
	pressed_index = -1;
	mouse_hover_index = -1;
	request_repaint();

}

void StartMenu::left_button_down(CL_InputEvent &input_event)
{
	SoundEngine::instance()->playClick();
	pressed_index = mouse_hover_index;
	request_repaint();

}
void StartMenu::left_button_up(CL_InputEvent &input_event)
{
		if(pressed_index != -1){
			if(pressed_index == 0){
				set_enabled(false); //disables StarMenu
				SoundEngine::instance()->stopMusic();
				World* world = new World(window, resources);
				SoundEngine::instance()->setMusic("sounds/Crusade.ogg");
				world->run("level1.xml");
				ObjectManager::instance()->releaseObjects();
				GameObject::reset_id_count();
				GraphicEngine::instance()->removeAllSprites();
				Stats::instance()->setKills(1,0);
				Stats::instance()->setKills(2,0);
				delete world;
				SoundEngine::instance()->stopAllSounds();
				SoundEngine::instance()->stopMusic();
				SoundEngine::instance()->setMusic("sounds/SoldiersAndPilgrims.ogg");
				set_enabled(true);
				request_repaint();
			
			
			}
			else if (pressed_index == 1){
				set_enabled(false);
				CL_ResourceManager resources;
				resources.load("graphics/GUIThemeLuna/resources.xml");
				CL_GUIManager gui2(window, "graphics/GUIThemeLuna");
				OptionsMenu  opmenu(&gui2, window.get_geometry(), window);
				set_enabled(true);
				
			}

			else if(pressed_index == 2){
				exit_with_code(1);
			}
		}
		

	pressed_index = -1;
	request_repaint();

}
