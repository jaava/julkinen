/*
 * this is a class for backround graphics
 * doesnt interact with others
 * clouds, birds, mountains etc.
 */
 
#include "gameobject.h"

class GraphicObject: public GameObject {
public:
	GraphicObject(size_t id, int type, int sprite, CL_Pointf pos): GameObject(id, type, pos) {
		is_active = false;
		alive = true;
		sprite_id = sprite;
		angle = CL_Angle(0, cl_degrees);
	}
	
	void update() {
		//nothing
	}
	
	void doCollide(GameObject* obj) {
		//doesnt collide
	}
	
};
