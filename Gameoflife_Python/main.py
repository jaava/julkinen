
# this is a simple implementation of conways game of life using random starting setting in a large matrix.
# pygame is used for opening a window and drawing the game progress.


import pygame
import random
import sys


pygame.init()

#define the colors: black and white cell
BC = pygame.Color(0,0,0)
WC = pygame.Color(255,255,255)


def main():

	global SIZE
	global DISP

	SIZE, numofIters, ratio = askParameters()# the parameters

	DISP = pygame.display.set_mode((SIZE,SIZE))#define the window

	world1 = []#The world is a SIZExSIZE matrix of zeros or ones
	world2 = []#Creates 2 worlds. Read one, write other
	for i in range(SIZE):
		world1.append([])
		world2.append([])
		for j in range(SIZE):
			if random.random() < ratio:
				world1[i].append(1)
				world2[i].append(1)
			else:
				world1[i].append(0)
				world2[i].append(0)

	play(world1, world2, numofIters)

	print "thanksbye"
	raw_input("quit")


def play(world1, world2, numofIters):
	
	DISP.fill(WC)
	pixar = pygame.PixelArray(DISP)#a pixel array to be shown	
	#draw initial world
	for i in range(SIZE):
		for j in range(SIZE):
			if world1[i][j] == 1:
				pixar[i][j] = BC
	del pixar
	pygame.display.update()

	print "lets start"
	
	for n in range(numofIters):
		
		DISP.fill(WC)
		pixar = pygame.PixelArray(DISP)

		if n%2 == 0:#flips the 2 worlds -- reads one and writes to the other
			update(world1, world2, pixar)
		else:
			update(world2, world1, pixar)
		
		print n+1
		del pixar
		pygame.display.update()


def update(world1, world2, pixar):
	#calls check for every cell and updates the pixel array
	for i in range(SIZE):
		for j in range(SIZE):
			check(i,j, world1, world2)
			if world2[i][j] == 1:
				pixar[i][j] = BC



def check(i,j,world1, world2):
	#counts how many neighbors a cell has and updates changes to the world2
	count = 0
	if i > 0 and i < SIZE-1 and j > 0 and j < SIZE-1:
		for a in range(i-1,i+2):
			for b in range(j-1,j+2):
				if a != i or b != j:
					count += world1[a][b]

	if (count < 2 or count > 3):
		world2[i][j] = 0
	elif count == 3:
		world2[i][j] = 1
	else:
		world2[i][j] = world1[i][j]



def askParameters():
	size = -1
	while size < 0:
		try:
			size = int(raw_input("give the size of the arena\n(a positive integer value)\n"))
		except ValueError:
			print "You put in something not an integer"
		if size <= 0:
			print "your size is invalid"

	print
	iterations = -1
	while iterations < 0:
		try:
			iterations = int(raw_input("give the number of iterations\n(a positive integer value)\n"))
		except ValueError:
			print "You put in something not an integer"
		if iterations <= 0:
			print "your num of iters is invalid"

	print
	ratio = -1.0
	while ratio < 0 or ratio > 1:
		try:
			ratio = float(raw_input("give the random parameter for initial filling\n(a float between 0 and 1)\n"))
		except ValueError:
			print "You put in something not a number"
		if ratio < 0 or ratio > 1:
			print "your ratio is invalid"

	print
	return size, iterations, ratio
	



if __name__ == "__main__":
	main()




