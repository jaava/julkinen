/*
 * c++ course project triplane2
 * objectmanager.cc
 * created by Jaakko Vallinoja
 *
 */

#include "objectmanager.h"
#include <stdexcept>
#include <iostream>

//creates the objectmanager instance
ObjectManager ObjectManager::manager;

//registers parameter GameObject to the container map
void ObjectManager::registerObject(GameObject* ptr){
  container[ptr->ID()] = ptr;
}
//removes parameter GameObject from the container map and deletes it
void ObjectManager::removeObject(GameObject * ptr){
  container.erase(ptr->ID());
  delete ptr;
}
 
//removes and deletes all remaining objects
void ObjectManager::releaseObjects(){
  std::map<size_t, GameObject*>::iterator it = container.begin();
  while (it != container.end()){
    delete it->second;
    it++;
  }
  container.erase(container.begin(), it);
}
/*	update loop
*		Loops through the container map and calls update for every alive object
*		Deletes unalive objects
*/	
void ObjectManager::update(){
  std::map<size_t, GameObject*>::iterator it = container.begin();
  while(it != container.end()){
  	if (it->second->getAlive()) {
    	(it++)->second->update();
    }
    else {
    	removeObject((it++)->second);
    }
  }
}

/*	draw loop
*		Loops through the container map and calls GraphicEngine::doDraw() for every alive object
*/
void ObjectManager::draw() {
	std::map<size_t, GameObject*>::iterator it = container.begin();
	while(it != container.end()){
		if (it->second->getAlive()) {
			GraphicEngine::instance()->doDraw(it->second);
		}
		it++;
	}
}
 
//returns object from the container
GameObject* ObjectManager::getObjectFromID(size_t id){
  if(!container.count(id)) {throw CL_Exception("Accessing invalid id");}
  return container[id];
}

/*	Collision loop
*		Loops through the container map and calls collide for every active object with every active object as a parameter
*/
void ObjectManager::collide() {
	std::map<size_t, GameObject*>::iterator it = container.begin();
	std::map<size_t, GameObject*>::iterator jt = container.begin();
	while(it != container.end()){
		jt = container.begin();
		while (jt != container.end()) {
			if (it->second != jt->second) {
				if (it->second->is_active && jt->second->is_active) {
					it->second->doCollide(jt->second);
				}
			}
			jt++;
		}
		it++;
	}
}

