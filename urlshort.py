# a simple url shortener web service without the database inplementation at the moment
# stores the url and gives its store index as a base 64 to make it shorter
# by jaava


from flask import Flask, abort, redirect, request

class IDcoder():
	#creates short ids with letters and numbers (basically just a base 64 converter) 
	def __init__(self):
		self.ALPHA = 'abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ'
		self.BASE = len(self.ALPHA)
	def encode(self, numbr):
		#creates a short string from an ID number
		strng = ''
		while numbr > 0:
			strng = self.ALPHA[numbr % self.BASE] + strng
			numbr = numbr / self.BASE
		return strng
	def decode(self, strng):
		#converts a string to an ID number
		numbr = 0
		for i in range(len(strng)):
			numbr = numbr * self.BASE + self.ALPHA.index(strng[i])
		return numbr

db = []#in memory database here is just a list
coder = IDcoder()

#value = {#this is here for testing. not needed.
#	'id': 1,
#	'url':'http://localhost:5000/'
#}
#db.append(value)

def add_to_database(string):
	#adds a new url to the database. Returns the of the new database value.
	if len(db) == 0:
		nextid = 1
	else:
		nextid = db[-1]['id'] + 1
	value = {
		'id': nextid
		'url': string
	}
	db.append(value)
	return nextid


app = Flask(__name__)

@app.route('/')
def index():
	return "This is an index for the url shortener"

@app.route('/<string:address>', methods=['GET'])
def get_url(address):
	ID = coder.decode(address)
	value = filter(lambda v: v['id'] == ID, db)
	if len(value)==0:
		abort(404)
	else:
		url = value[0]['url']
		return redirect(url, 301)

@app.route('/', methods=['POST'])
def post_new():
	data = request.form.keys()[0]#assume there is only the url in plain text and nothing else in the data.
	value = filter(lambda v: v['url'] == data, db)
	if len(value) != 0:
		result = coder.encode(value[0]['id'])
	else:
		ID = add_to_database(data)
		result = coder.encode(ID)
	#result = 'http://localhost:5000/'+result
	return result


if __name__ == '__main__':
	app.run()





