
#pragma once

#include <ClanLib/core.h>
#include <ClanLib/display.h>
#include <ClanLib/gl.h>
#include <ClanLib/application.h>

#include "graphicengine.h"
#include "world.hh"
#include "defines.h"
//#include "precomp.h"
#include <iostream>

class Program
{
public:
	static int main(const std::vector<CL_String> &args);
};

