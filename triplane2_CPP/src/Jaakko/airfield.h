/*
 *	airfield.h
 *	c++ course project triplane2
 */

#ifndef CPP_PROJECT_AIRFIELD
#define CPP_PROJECT_AIRFIELD

#include "defines.h"
#include "gameobject.h"

/*	acts as a base for planes
*		parameters: id, type, side, position
*/
class Airfield: public GameObject 
{
public:
		Airfield(size_t id, int type, int side, CL_Pointf pos): GameObject(id, type, pos)
	{
		sprite_id = AIRFIELD_SPRITE;
		is_active = true;
		angle = CL_Angle(0, cl_degrees);
		faction = side;
		alive = true;
		
		outline = new CL_CollisionOutline("graphics/airfield_base.png");
		outline->set_alignment(origin_bottom_left);
		outline->set_translation(pos.x, pos.y);
	}
	
	void update() {
		//
	}
	
	void doCollide(GameObject* obj) {
		if (obj->getType() == TERRAIN) {
			if (obj->getOutline().point_inside(CL_Pointf(pos.x, pos.y-7))) {
				pos.y--;
				outline->set_translation(pos.x, pos.y);
			}
		}
	}
	
	
private:
	
};


#endif
