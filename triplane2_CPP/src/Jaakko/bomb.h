/*
 *	bomb.h
 *	c++ project triplane2
 */

#ifndef CPP_PROJECT_BOMB
#define CPP_PROJECT_BOMB
#include "gameobject.h"
#include "graphicengine.h"
#include "soundengine.h"
#include <iostream>

/*	bomb, dropped by a plane
*		parameters: id, type, angle, velocity, owner, position
*/
class Bomb: public GameObject {
public:
	Bomb(size_t id, int type, CL_Angle _angle, CL_Pointf v, GameObject* _owner, CL_Pointf pos): GameObject(id, type, pos) , v(v)
	{
		// Fixes the starting angle 		
		angle =  _angle.normalize();
		if (angle.to_degrees() < 90 && angle.to_degrees() > 0)
			angle+= CL_Angle(90.0f,cl_degrees);
		else
			angle -= CL_Angle(270, cl_degrees);
		v.y += 5;
		sprite_id = GraphicEngine::instance()->createSprite(this, BOMB_SPRITE);
		is_active = false;
		alive = true;
		atime = CL_System::get_time();
		is_destroying = false;
		second = false;
		owner = _owner;
		sound_id = SoundEngine::instance()->playSound(this, "BombDrop", false);
	}
	
	void update() {
		if (CL_System::get_time() > atime + 700 && !is_active) {
			is_active = true;
		}
		
		v.y += 1.5 * GRAVITY * 0.001 * dt;


		pos.x += v.x * 0.001*dt;
		pos.y += v.y * 0.001*dt;

		if (v.x > 0) {
			if (angle < CL_Angle((atan(v.y/v.x)+PI/2),cl_radians)) {
				angle +=  CL_Angle(1,cl_degrees);
			}
			else {
				angle -= CL_Angle(1,cl_degrees);
			}
		}
		else {
			if (angle < CL_Angle((atan(v.y/v.x)-PI/2),cl_radians)) {
				angle +=  CL_Angle(1,cl_degrees);
			}
			else {
				angle -= CL_Angle(1,cl_degrees);
			}
		}
		if (is_destroying) {
			if (second) {
				is_active = false;
			}
			second = true;
			v.x = 0;
			v.y = 0;
			if (GraphicEngine::instance()->getSprite(this)->is_finished()) {
				alive = false;
				GraphicEngine::instance()->removeSprite(this);
				return;
			}
		}
		
		//makes bombs disappear if they somehow go below screen
		if (pos.y > HEIGHT) {
			alive = false;
			GraphicEngine::instance()->removeSprite(this);
			return;
		}
	}
	
	void doCollide(GameObject* obj) {
		switch (obj->getType()) {
			case TERRAIN: 
			case PLANE: 
			case AIRFIELD:
			case BARRACKS:
			case AABASE:
			case PELICAN: {
				if (obj->getOutline().point_inside(pos) && !second) {
					is_destroying = true;
					GraphicEngine::instance()->removeSprite(this);
					sprite_id = GraphicEngine::instance()->createSprite(this, EXPLOSION2);
					SoundEngine::instance()->stopSound(sound_id);
					SoundEngine::instance()->playSound(this,"BombExplosion", false);
					atime = CL_System::get_time();
				}
				break;
			}
			case BULLET: 
			case BOMB: {
				if (obj->getPos().x == pos.x && obj->getPos().y == pos.y && !second) {
					is_destroying = true;
					GraphicEngine::instance()->removeSprite(this);
					sprite_id = GraphicEngine::instance()->createSprite(this, EXPLOSION2);
					SoundEngine::instance()->stopSound(sound_id);
					SoundEngine::instance()->playSound(this,"BombExplosion", false);
					atime = CL_System::get_time();
				}
				break;
			}
			default: {break;}
		}
	}
	
private:
	CL_Pointf v;
	size_t atime;
	float velocity;
	bool second;
	GameObject* owner;
	size_t sound_id; 
};

#endif
