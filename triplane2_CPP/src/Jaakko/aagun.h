/*
 *	aagun.h
 *	aagun, aabase and aabullet classes
 *	c++ course project triplane2
 */
 
#ifndef CPP_PROJECT_AAGUN
#define CPP_PROJECT_AAGUN

#include "gameobject.h"

/*	aa granade class ---------------------------------------------
*		created by aagun
* 	parameters: id, type, angle, _t_y (target height), position
*/
class AaBullet: public GameObject {
public:
	AaBullet(size_t id, int type, CL_Angle _angle, float _t_y, CL_Pointf pos): GameObject(id, type, pos) {
		angle = _angle;
		velocity = 500; 
		v.y = sin((-1)*angle.to_radians())*velocity;
		v.x = cos((-1)*angle.to_radians())*velocity;
		type = AABULLET;		
		is_active = false;
		sprite_id = BULLET_SPRITE_RED;
		atime = CL_System::get_time();
		is_destroying = false;
		alive = true;
		t_y = _t_y;
	}
	
	void update() {
		v.y -= 50 * 0.001 * dt;
		pos.x += v.x * 0.001*dt;
		pos.y -= v.y * 0.001*dt;
		if (!is_active) {
			if (CL_System::get_time() > atime + 200) {
				is_active = true;
			}
		}
		
		if (is_destroying) {
			is_active = false;
			if (CL_System::get_time() > atime+300) {
				alive = false;
				GraphicEngine::instance()->removeSprite(this);
				return;
			}
		}
		
		if (CL_System::get_time() > atime + 2000) {
			is_destroying = true;
			sprite_id = GraphicEngine::instance()->createSprite(this, EXPLOSION1);
			SoundEngine::instance()->playSound(this, "BombExplosion", false);
			atime = CL_System::get_time();
		}
		
		if (pos.y < t_y + 2 && is_active) {
			is_destroying = true;
			sprite_id = GraphicEngine::instance()->createSprite(this, EXPLOSION1);
			SoundEngine::instance()->playSound(this, "BombExplosion", false);
			atime = CL_System::get_time();
		}
	}
	
	void doCollide(GameObject* obj) {
		switch (obj->getType()) {
			case PLANE: {
				if (obj->getOutline().point_inside(pos)) {
					is_destroying = true;
					SoundEngine::instance()->playSound(this, "BombExplosion", false);
					sprite_id = GraphicEngine::instance()->createSprite(this, EXPLOSION1);
					atime = CL_System::get_time();
				}
				break;
			}
			case TERRAIN:
			case AIRFIELD:
			case BARRACKS:
			case SOLDIER: {
				if (obj->getOutline().point_inside(pos)) {
					is_destroying = true;
					sprite_id = GraphicEngine::instance()->createSprite(this, EXPLOSION1);
				}
				break;
			}
			default: {break;}
		}
	}
	
private:
	CL_Pointf v;
	size_t atime;//used for activation and deactivation
	float velocity;
	float t_y;
};


/*	aagun class---------------------------------------------------------------
*		parameters: id, type, side, position
*		shoots aabullets to enemy plane, moving part of the gun
*/
class AaGun: public GameObject {
public:
	AaGun(size_t id, int type, int _faction, CL_Pointf pos): GameObject(id, type, pos) {
		faction = _faction;
		flip=false;
		is_active = true;
		firing = false;
		fire = false;
		alive = true;
		is_destroying = false;
		angle = CL_Angle(40, cl_degrees);
		sprite_id = GraphicEngine::instance()->createSprite(this, AAGUN_SPRITE);
		atime = CL_System::get_time();
		
	}
	
	void update() {
		if (angle.normalize_180().to_degrees() <= 90 && angle.normalize_180().to_degrees() >= -90 && flip  && !fire && !firing) {
			GraphicEngine::instance()->removeSprite(this);
			sprite_id = GraphicEngine::instance()->createSprite(this, AAGUN_SPRITE);
			flip = false;
		}
		else if (angle.normalize().to_degrees() > 90 && angle.normalize().to_degrees() < 270 && !flip && !fire && !firing) {
			GraphicEngine::instance()->removeSprite(this);
			sprite_id = GraphicEngine::instance()->createSprite(this, AAGUNFLIP_SPRITE);
			flip = true;
		}
		else if (angle.normalize_180().to_degrees() <= 90 && angle.normalize_180().to_degrees() >= -90 && fire && !firing) {
			GraphicEngine::instance()->removeSprite(this);
			sprite_id = GraphicEngine::instance()->createSprite(this, AAGUNFIRE);
			flip = false;
			firing = true;
			fire = false;
		}
		else if (angle.normalize().to_degrees() > 90 && angle.normalize().to_degrees() < 270 && fire && !firing) {
			GraphicEngine::instance()->removeSprite(this);
			sprite_id = GraphicEngine::instance()->createSprite(this, AAGUNFIREFLIP);
			flip = true;
			firing = true;
			fire = false;
		}
		
		else if (!fire && !firing) {
			if (angle.normalize_180() < CL_Angle(45, cl_degrees)) {
				angle += CL_Angle(0.1, cl_degrees);
			}
			else if (angle.normalize_180() > CL_Angle(45, cl_degrees)) {
				angle -= CL_Angle(0.1, cl_degrees);
			}
		}
		
		if (firing && CL_System::get_time() > atime + 200) {
			firing = false;
			if (angle.normalize_180().to_degrees() <= 90 && angle.normalize_180().to_degrees() >= -90) {
				GraphicEngine::instance()->removeSprite(this);
				sprite_id = GraphicEngine::instance()->createSprite(this, AAGUN_SPRITE);
				flip = false;
			}
			else if (angle.normalize().to_degrees() > 90 && angle.normalize().to_degrees() < 270) {
				GraphicEngine::instance()->removeSprite(this);
				sprite_id = GraphicEngine::instance()->createSprite(this, AAGUNFLIP_SPRITE);
				flip = true;
			}
		}
		
		
		if (is_destroying) {
			GraphicEngine::instance()->removeSprite(this);
			alive = false;
			return;
		}
	}
	
	void doCollide(GameObject* obj) {
		switch (obj->getType()) {
			
			//shooting
			case PLANE: {
				float distance = sqrt(pow(pos.x - obj->getPos().x, 2) + pow(pos.y - obj->getPos().y, 2));
				if (obj->faction != faction && distance < AARANGE) {
					float fangle = atan((obj->getPos().y - pos.y)/(obj->getPos().x - pos.x));
					angle = CL_Angle(fangle, cl_radians);
					if (angle > CL_Angle(0, cl_radians)) {angle += CL_Angle(180, cl_degrees);}
					fireAaBullet(angle, obj);
					angle += CL_Angle(180, cl_degrees);
				}
				break;
			}
			
			default: {break;}
		}
	}
	
	/*	shoots aabullet
	*		parameters: directiong angle, target object
	*/
	void fireAaBullet(CL_Angle _angle_, GameObject* obj) {
		if (CL_System::get_time() > atime+1500) {
			AaBullet* bullet = new AaBullet(GameObject::getNextID(), AABULLET, _angle_, obj->getPos().y, pos);
			ObjectManager::instance()->registerObject(bullet);
			SoundEngine::instance()->playSound(this, "BombExplosion2", false);//new sound needed
			atime = CL_System::get_time();
			fire = true;
		}
	}
	
	//aabase interface
	//sets object status to is_destroying
	void setDestroying() {
		is_destroying = true;
	}
	//movest gun up
	void setYpos() {
		pos.y--;
	}
	
private:
	size_t atime;
	bool flip;
	bool firing;
	bool fire;
};

/*	base of the gun -_-_-_----------------------------------------------------------------
*		static part of the gun, takes care of the aagun pointer
*		parameters: id, type, side, aagun (pointer to the moving part of this gun unit), position
*/
class AaBase: public GameObject {
public:
	AaBase(size_t id, int type, int _faction, AaGun* _gun, CL_Pointf pos): GameObject(id, type, pos) {
		faction = _faction;
		is_active = true;
		angle = CL_Angle(0, cl_degrees);
		sprite_id = AABASE_SPRITE;
		is_destroying = false;
		alive = true;
		
		gun = _gun;
		
		outline = new CL_CollisionOutline("graphics/turretBase.png");
		outline->set_alignment(origin_bottom_center);
		outline->set_translation(pos.x, pos.y);
	}
	
	void update() {
		
		if (is_destroying) {
			if (is_active) {
				sprite_id = GraphicEngine::instance()->createSprite(this, AABASEDESTROY_SPRITE);
			}
			is_active = false;
			if (GraphicEngine::instance()->getSprite(this)->is_finished()) {
				GraphicEngine::instance()->removeSprite(this);
				sprite_id = AABASEDESTROY_SPRITE;
			}
			if (gun) {
				gun->setDestroying();
				gun = NULL;
			}
		}
	}
	
	void doCollide(GameObject* obj) {
		switch (obj->getType()) {
			case BOMB: {
				if (outline->point_inside(CL_Pointf(obj->getPos().x, obj->getPos().y))) {
					is_destroying = true;
				}
				break;
			}
			case TERRAIN: {
				if(obj->getOutline().point_inside(CL_Pointf(pos.x, pos.y-3))) {
					pos.y--;
					gun->setYpos();
					outline->set_translation(pos.x, pos.y);
				}
			}
			default: {break;}
		}
	}
	
	void setGun(AaGun* _gun) {
		gun = _gun;
	}
	
	~AaBase() {delete outline;}
	
private:
	AaGun* gun;
};


#endif
