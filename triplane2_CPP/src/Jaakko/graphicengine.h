/*
 * c++ course project triplane2
 * graphicengine.hh
 * created by jaakko vallinoja
 */
#ifndef CPP_PROJECT_GRAPHICENGINE
#define CPP_PROJECT_GRAPHICENGINE

#include "defines.h"
#include "gameobject.h"
#include "objectmanager.h"
#include "stats.h"

#include <map>
#include <string>

class GameObject;
 //singleton class GraphicEngine
class GraphicEngine
{
public:
	static GraphicEngine* instance() {return &ge;}
	
	//draws information to the right side of the screen
	void drawStats();
	
	//draws everything
	void draw();
	
	void doDraw(GameObject* object);//needs objects first
	
	void setgraphics(CL_GraphicContext pargc, CL_DisplayWindow _window);
	void setResources(const CL_ResourceManager& _man);
	CL_Sprite* getSprite (GameObject* object);
	
	void staticSprites();
	void setPlayers(GameObject* p1, GameObject* p2 = NULL);
	
	size_t createSprite(GameObject*, int);
	void removeSprite(GameObject*);
	void removeAllSprites();
	void drawResult();
	
	~GraphicEngine() {removeAllSprites();}
private:
	GraphicEngine() {}
	GraphicEngine(const GraphicEngine&);
	GraphicEngine& operator=(const GraphicEngine&);
	
	static GraphicEngine ge;
	CL_GraphicContext gc;
	CL_DisplayWindow window;
	CL_ResourceManager resources;
	
	//players (for knowing what to follow)
	GameObject* pl1;
	GameObject* pl2;
	
	
	//sprites
	std::map<size_t , CL_Sprite*> sprites;
	//CL_Sprite* plane1;
};

#endif
