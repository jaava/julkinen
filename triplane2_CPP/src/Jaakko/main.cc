/*
 * main program file 
 * class Program
 * c++ course project triplane2
 * created by jaakko vallinoja
 *
 */

//#include "precomp.h"
#include "defines.h"
#include "graphicengine.h"
#include "world.h"
#include "menus.h"
#include "soundengine.h"
#include "input.h"
#include <iostream>

class Program
{
public:
  static int main(const std::vector<CL_String> &args) {
	//initialise core elements
	CL_ConsoleWindow console("Console", 80, 1000);
	CL_ConsoleLogger logger;
	
	CL_SetupCore setup_core;
	CL_SetupDisplay setup_display;
  #ifdef USEGL1
		CL_SetupGL1 setup_gl1;
	#endif
	#ifdef USEGL2
		CL_SetupGL setup_gl;
	#endif
	#ifdef USESWR
		CL_SetupSWRender setup_swrender;
	#endif
	CL_SetupGUI setup_gui;

	// Initialize the sound system
	CL_SetupSound setup_sound;

	// Initialize mikmod
	CL_SetupMikMod setup_mikmod;

	// Initialize vorbis
	CL_SetupVorbis setup_vorbis;

	//opens window and gives window and gc and resources to graphics engine
	CL_DisplayWindow window("SkyTerror", WIDHT, HEIGHT);
	CL_GraphicContext gc = window.get_gc();
	GraphicEngine::instance()->setgraphics(gc, window);
	//resources for graphicengine
	CL_ResourceManager resources("resources.xml");
	GraphicEngine::instance()->setResources(resources);
	ObjectManager::instance()->setResources(resources);
	SoundEngine::instance()->init();
	//CL_SoundOutput output(44100);
	CL_GUIManager gui(window, "graphics/Gui");
	Input::instance()->init(window);

    //start the menu
    try {
		StartMenu menu(&gui, window.get_viewport(), window, resources);
		
		menu.run();
    }
    catch(CL_Exception &ex) {
      //opens console window for text output
      CL_ConsoleWindow console("Triplane2 Console", 80, 160);
      CL_Console::write_line("Error: " + ex.get_message_and_stack_trace());
      console.display_close_message();
      return -1;
    }

    return 0;
  }
};

//main program
CL_ClanApplication app(&Program::main);
