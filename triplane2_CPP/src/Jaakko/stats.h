/*
 * stats.h
 * c++ course project triplane2
 *
 */
 
#ifndef CPP_COURSE_STATS
#define CPP_COURSE_STATS

#include "defines.h"
#include "graphicengine.h"

//singleton class that keeps track on players data
class Stats {
public:
	static Stats* instance() {return &stats;}
	
	void addKill(int pl);
	void reduceKill(int pl);
	void setKills(int pl, int kills);
	void addVictory();
	
	void setAmmo(int pl, int ammo);
	
	void setFuel(int pl, int fuel);
	
	void setPelicans(int pl, int fuel);
	
	void setBombs(int pl, int bombs);
	//sets all stats
	void setAllStats(int pl, int bombs, int fuel, int ammo, int pelicans);
	
	int getBombs(int pl) const;
	
	int getFuel(int pl) const;
	
	int getAmmo(int pl) const;
	
	int getPelicans(int pl) const;
	
	size_t getKills(int pl) const;
	

private:
	Stats () {}
	Stats (const Stats&);
	Stats& operator=(const Stats&); 
	static Stats stats;
	
	//variables
	size_t pl1kills;
	size_t pl2kills;
	size_t pl1victories;
	size_t pl2victories;
	int pl1fuel;
	int pl2fuel;
	int pl1ammo;
	int pl2ammo;
	int pl1bombs;
	int pl2bombs;
	int pl1pelicans;
	int pl2pelicans;

};

#endif
