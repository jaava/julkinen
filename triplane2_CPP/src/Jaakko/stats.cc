/*
 * stats.cc
 * c++ course project triplane2
 *
 */
 
#include "stats.h"

Stats Stats::stats;

void Stats::addKill(int pl) {
		if (pl == 1) {pl2kills++;}
		else {pl1kills++;}
	}
void Stats::reduceKill(int pl) {
	if (pl == 1) {pl1kills--;}
	else {pl2kills--;}
}
void Stats::setKills(int pl, int kills) {
	if (pl == 1) {
		pl1kills = kills;
	}
	else {
		pl2kills = kills;
	}
}

void Stats::addVictory() {
	if (pl1kills > pl2kills) {pl1victories++;}
	else if (pl2kills > pl1kills) {pl2victories++;}
}
	
void Stats::setAmmo(int pl, int ammo) {
	if (pl == 1) {
		pl1ammo = ammo;
	}
	else {
		pl2ammo = ammo;
	}
}

void Stats::setFuel(int pl, int fuel) {
	if (pl == 1) {
		pl1fuel = fuel;
	}
	else {
		pl2fuel = fuel;
	}
}

void Stats::setPelicans(int pl, int pelicans) {
	if (pl == 1) {
		pl1pelicans = pelicans;
	}
	else {
		pl2pelicans = pelicans;
	}
}
	
void Stats::setBombs(int pl, int bombs) {
	if (pl == 1) {
		pl1bombs = bombs;
	}
	else {
		pl2bombs = bombs;
	}
}
//sets all stats
void Stats::setAllStats(int pl, int bombs, int fuel, int ammo, int pelicans) {
	if (pl == 1) {
		pl1bombs = bombs;
		pl1ammo = ammo;
		pl1fuel = fuel;
		pl1pelicans = pelicans;
	}
	else {
		pl2bombs = bombs;
		pl2ammo = ammo;
		pl2fuel = fuel;
		pl2pelicans = pelicans;
	}
}

int Stats::getBombs(int pl) const {
	if (pl == 1) {
		return pl1bombs;
	}
	else {
		return pl2bombs;
	}
}

int Stats::getFuel(int pl) const {
	if (pl == 1) {
		return pl1fuel;
	}
	else {
		return pl2fuel;
	}
}

int Stats::getPelicans(int pl) const {
	if (pl == 1) {
		return pl1pelicans;
	}
	else {
		return pl2pelicans;
	}
}

int Stats::getAmmo(int pl) const {
	if (pl == 1) {
		return pl1ammo;
	}
	else {
		return pl2ammo;
	}
}

size_t Stats::getKills(int pl) const {
	if (pl == 1) {
		return pl1kills;
	}
	else {
		return pl2kills;
	}
}

