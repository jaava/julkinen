/*
 *	barracs.h
 *	cpp project triplane2
 */

#ifndef CPP_PROJECT_BARRACKS
#define CPP_PROJECT_BARRACKS

#include "gameobject.h"
#include "soldier.h"
#include <cmath>
#include <iostream>

/*	building for spawnig more soldiers
*		soldiers spawn on random intervals from this building
*		parameters: id, type, side, position
*/
class Barracks: public GameObject {
public:
	Barracks(size_t id, int type, int side, CL_Pointf pos): GameObject(id, type, pos) {
		faction = side;
		sprite_id = BARRACKS_SPRITE;
		is_active = true;
		alive = true;
		angle = CL_Angle(0, cl_degrees);
		spawn = rand()/700000;
		is_destroying = false;
		
		outline = new CL_CollisionOutline("graphics/Barracks.png");
		outline->set_alignment(origin_bottom_left);
		outline->set_translation(pos.x, pos.y);
		outline->set_scale(0.6,0.6);
	}
	
	
	//update spawns soldiers on random intervals
	void update() {
		if (is_active) {
			if (spawn < 0) {
				spawn = rand() / 700000;
				spawnSoldier();
			}
			spawn--;
		}
		if (is_destroying) {
			is_active = false;
			sprite_id = BARRACKSD_SPRITE;
		}
		
	}
	
	void doCollide(GameObject* obj) {
		switch (obj->getType()) {
			case BOMB: {
				if (outline->point_inside(obj->getPos())) {
					is_destroying = true;
					
					break;
				}
			}
			default: {break;};
		}
	}
	
	~Barracks() {delete outline;}
	
	void spawnSoldier() {
		Soldier* soldier = new Soldier(GameObject::getNextID(), SOLDIER, faction, pos);
		ObjectManager::instance()->registerObject(soldier);
	}
	
private:
	int spawn;
};

#endif
