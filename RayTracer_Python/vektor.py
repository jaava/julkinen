

#vektor - own implementation of vector math. just for the fun of it.
#of course there would have been ready implementations but this was fun.
#by jaava
from math import *

class Vektor:
    def __init__(self, x,y,z):
        self.x = x
        self.y = y
        self.z = z
    
    def __str__(self):
        a = str(self.x)
        b = str(self.y)
        c = str(self.z)
        return "x: "+a+" y: "+b+" z: "+c
    
    def set(self, x,y,z):
        self.x = x
        self.y = y
        self.z = z

	#addition
    def plus(self, other):
        x = self.x + other.x
        y = self.y + other.y
        z = self.z + other.z
        return Vektor(x,y,z)

	#subtraction
    def minus(self, other):
        x = self.x - other.x
        y = self.y - other.y
        z = self.z - other.z
        return Vektor(x,y,z)

	#dot product
    def dotpr(self, other):
        sum = 0
        sum += self.x * other.x
        sum += self.y * other.y
        sum += self.z * other.z
        return sum
	
	#scalar product
    def scapr(self, sca):
        x = self.x * sca
        y = self.y * sca
        z = self.z * sca
        return Vektor(x,y,z)
	
	#normalize to length 1
    def norm(self):
        d = sqrt(self.x*self.x + self.y*self.y + self.z*self.z)
        self.x = self.x / d
        self.y = self.y / d
        self.z = self.z / d
	
	#distance of two points
    def distance(self, other):
        d = sqrt((self.x-other.x)*(self.x-other.x) + (self.y-other.y)*(self.y-other.y) + (self.z-other.z)*(self.z-other.z))
        return d