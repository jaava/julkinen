/*
 * defines.h
 * holds define values and some core includes
 * written by jaakko vallinoja
 */
#ifndef CPP_PROJECT_DEFINES
#define CPP_PROJECT_DEFINES

//#define USEGL1
#define USEGL2
//#define USESWR

#include <ClanLib/core.h>
#include <ClanLib/display.h>
#include <ClanLib/application.h>
#include <ClanLib/gui.h>
#include <ClanLib/sound.h>
#include <ClanLib/mikmod.h>
#include <ClanLib/vorbis.h>

#ifdef USEGL1
#include <ClanLib/gl1.h>
#endif
#ifdef USEGL2
#include <ClanLib/gl.h>
#endif
#ifdef USESWR
#include <ClanLib/swrender.h>
#endif
 
//screen properties
#define WIDHT 1024
#define HEIGHT 768
#define MAPWIDHT 4000

//play area properties //mostly for crating testing boundaries
#define MINY 384 //sky limit. 
#define MAXY 768 //bottom of the screen. useless as we have terrain
#define MINX 0
#define MAXX 4000 //must be same as the mapwidht. at least in the testing phase

//nature/plane
#define GRAVITY 50
#define PI 3.141
#define PLANE_MASS 700
#define TURN 120
#define ENGINE_POWER 80000
#define LIFT_CL 1.1
#define AIR_CL 2.5

#define AMMO 200;
#define BOMBS 10;
#define PELICANS 5;
#define FUEL 100;
#define HP 100;

//soldier shooting range
#define RANGE 300
#define AARANGE 500

//enumerations

//types
enum {
	TERRAIN,//0
	PLANE,
	BULLET,
	BOMB,
	AIRFIELD,
	SOLDIER,
	PELICAN,
	BARRACKS,
	AAGUN,
	AABULLET,
	AABASE,//10
	GRAPHICOBJECT
};
	
//sprites
enum {
	TERRAIN_SPRITE,//0
	PLANE1_SPRITE,
	PLANE2_SPRITE,
	PLANE1FLIP_SPRITE,
	PLANE2FLIP_SPRITE,
	FLIPANIMATION1,
	FLIPANIMATIONBACK1,
	FLIPANIMATION2,
	FLIPANIMATIONBACK2,
	BULLET_SPRITE,
	BOMB_SPRITE,//10
	BOMB_SPRITE2,
	EXPLOSION1,
	EXPLOSION2,
	EXPLOSION3,
	EXPLOSION4,
	AIRFIELD_SPRITE,
	SOLDIERWALKINGLEFT,
	SOLDIERWALKINGRIGHT,
	SOLDIERFIRINGLEFT,
	SOLDIERFIRINGRIGHT,//20
	SOLDIERDIELEFT,
	SOLDIERDIERIGHT,
	PELICAN_SPRITE,
	PELICAN_SPRITE2,
	PELICANLEFT,
	PELICANRIGHT,
	BARRACKS_SPRITE,
	BARRACKSD_SPRITE,
	GUIBACKROUND,
	AIRFIELDBACK_SPRITE,//30
	FLAG_SPRITE,
	FLAG2_SPRITE,
	BULLET_SPRITE_RED,
	PELICANDIER,
	PELICANDIEL,
	AABASE_SPRITE,
	AAGUN_SPRITE,
	AAGUNFLIP_SPRITE,
	AAGUNFIRE,
	AAGUNFIREFLIP,//40
	AABASEDESTROY_SPRITE,
	PALM1,
	PALM2,
	CLOUD1,
	CLOUD2,
	TERRAIN_SPRITE2
};



 
#endif
