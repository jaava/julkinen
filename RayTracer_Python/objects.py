

#objects
#implementations of of the classes needed to define a visible object in the picture
#currently only planes and balls and light sources
#by jaava

from defines import *
from vektor import *


class Light:
    def __init__(self, pos, pow = 1):
        self.pos = pos
        self.pow = pow





class Ball:
    def __init__(self, type, pos, r, mat):
        self.type = type
        self.pos = pos
        self.r = r
        self.mat = mat


class Plane:
    def __init__(self, type, pos, s, mat):
        self.type = type
        self.pos = pos
        self.s = s
        self.mat = mat


class Box:#not ready
    def __init__(self, type, pos, s, mat):
        self.type = type
        self.pos = pos
        self.s = s
        self.mat = mat



class Material:

    def __init__(self, col, ref = 0, n = 1, tra = 0, lig = 0):
        self.col = col
        self.n = n
        self.ref = ref
        self.tra = tra
        self.lig = lig


