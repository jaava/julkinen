/*
 *  gameobject.hh
 *  c++ course project triplane2
 *
 *  Created by Jaakko Vallinoja
 *	GameObject has the interface objects need to interact with each other and the engines
 *	Every gameobject inherit this and uses the same interface
 */

#ifndef CPP_PROJECT_GAMEOBJECT
#define CPP_PROJECT_GAMEOBJECT

#include "defines.h"
#include "objectmanager.h"

#include <iostream>


class GameObject
{
public:
	explicit GameObject(size_t id, int type, CL_Pointf pos): 
    id(id), type(type), pos(pos)
	{if (id < id_count){
		throw CL_Exception("GameObject creation: Invalid ID");
    }
		id_count++;
	}
	
	/*	sets static variable dt
	*		used by World to set same dt to every gameobject
	*		parameters: size_t 
	*/
	static void setTime(size_t newdt);
	/*	resets static id_count when quiting a game
	*/
	static void reset_id_count();
	
	/*	pure virtual update
	*/	
	virtual void update() = 0;
	/*	pure virtual collision detection
	*		parameters: another gameobject
	*/
	virtual void doCollide(GameObject*) = 0;
	
	virtual ~GameObject() {}
	
	//getters
	size_t ID() const {return id;}
	static size_t getNextID() {return id_count;}
	int getType() const {return type;}
	const CL_Angle& getAngle() {return angle;}
	const CL_CollisionOutline& getOutline() {return *outline;}
	bool getDestroying() {return is_destroying;}
	size_t getSpriteId() {return sprite_id;}
	const CL_Pointf& getPos() {return pos;}
	bool getAlive() {return alive;}
	
	//public variables
	int faction;
	bool is_active;
protected:
	static size_t dt; // (milliseconds)
	size_t id;
	int type;//type of gameobject (plane, soldier, bomb...)
	static size_t id_count;
	CL_CollisionOutline* outline;
	bool is_destroying;
	bool alive;
	CL_Angle angle;
	size_t sprite_id;
	CL_Pointf pos;

private:
	GameObject(GameObject const& other);
	GameObject& operator=(GameObject const& other);

	
};

#endif
