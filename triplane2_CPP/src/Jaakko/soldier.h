/*
 * soldier.h 
 * c++ course project
 *
 */
 
#ifndef CPP_PROJECT_SOLDIER
#define CPP_PROJECT_SOLDIER

#include "defines.h"
#include "gameobject.h"
#include "soundengine.h"
#include <cstdlib>
#include <cmath>
#include <iostream>



class Soldier: public GameObject
{
public:
	Soldier(size_t id, int type, int side, CL_Pointf pos): GameObject(id, type, pos) 
	{
		faction = side;
		die_sound = 0;
		walk = 0;
		v.x = 0;
		angle = CL_Angle(0.0, cl_degrees);
		is_destroying = false;
		is_active = true;
		alive = true;
		atime = CL_System::get_time();
		sprite_id = SOLDIERWALKINGLEFT;
		
		outline = new CL_CollisionOutline("graphics/soldierMoveRight/soldier2.png");
		outline->set_alignment(origin_center);
		outline->set_translation(pos.x, pos.y);
	}

	void update() {
		if (walk == 0 && CL_System::get_time() > atime+500 && is_active) {
			if (v.x == 0) {
				v.x = 20;
			}
			walk = rand() / 500000;
			v.x = v.x * (-1);
			if (v.x >= 0){sprite_id = SOLDIERWALKINGRIGHT;}
			else {sprite_id = SOLDIERWALKINGLEFT;}
		}
		if (walk >0) {walk--;}
		
		pos.x += v.x * 0.001*dt;
		pos.y += v.y * 0.001*dt;
		outline->set_translation(pos.x, pos.y);
		
		v.y += 0.1;
		
		if (pos.x > MAXX-100) {
			v.x = -20; 
			sprite_id = SOLDIERWALKINGLEFT;
			walk = 5000;
		}
		if (pos.x < MINX+100) {
			v.x = 20; 
			sprite_id = SOLDIERWALKINGRIGHT;
			walk = 5000;
		}
		
		if (is_destroying || pos.y > MAXY) {
			
				is_active = false;
				v.y = 0;
				v.x = 0;
			
			if(CL_System::get_time() > die_time + 7000){
				alive = false;
				GraphicEngine::instance()->removeSprite(this);
			}
			return;
		}
	}
	
	void doCollide(GameObject* obj) {
		switch (obj->getType()) {
			case TERRAIN: {
				if (obj->getOutline().point_inside(pos)) {
					v.y = 2;
					pos.y--;
				}
				break;
			}
			case BULLET: {
				if (outline->point_inside(obj->getPos())) {
					is_destroying = true;
					die_sound = SoundEngine::instance()->playSound(this,"SoldierDie",false);					
					die_time = CL_System::get_time();
					//GraphicEngine::instance()->removeSprite(this);
					if (sprite_id == SOLDIERWALKINGLEFT || sprite_id == SOLDIERFIRINGLEFT)
						sprite_id = GraphicEngine::instance()->createSprite(this, SOLDIERDIELEFT);
					else
						sprite_id = GraphicEngine::instance()->createSprite(this, SOLDIERDIERIGHT);
				}
				break;
			}
			//this handles targetting
			case PLANE: {
				float distance = sqrt(pow(pos.x - obj->getPos().x, 2) + pow(pos.y - obj->getPos().y, 2));
				if (obj->faction != faction && distance < RANGE) {
					if (obj->getPos().x > pos.x){
						sprite_id = SOLDIERFIRINGRIGHT;
					}
					else {
						sprite_id = SOLDIERFIRINGLEFT;
					}
					float fangle = atan((obj->getPos().y - pos.y)/(obj->getPos().x - pos.x));
					CL_Angle angle = CL_Angle(fangle, cl_radians);
					if (angle > CL_Angle(0, cl_radians)) {angle += CL_Angle(180, cl_degrees);}
					fireBullet(angle, obj);
				}
				break;
			}
			case BOMB: {
				float distance = sqrt(pow(pos.x - obj->getPos().x, 2) + pow(pos.y - obj->getPos().y, 2));
				if (obj->getDestroying() && distance < 65){
					is_destroying = true;
					die_sound = SoundEngine::instance()->playSound(this, "SoldierDie", false);
					die_time = CL_System::get_time();
					if (sprite_id == SOLDIERWALKINGLEFT || sprite_id == SOLDIERFIRINGLEFT)
						sprite_id = GraphicEngine::instance()->createSprite(this, SOLDIERDIELEFT);
					else
						sprite_id = GraphicEngine::instance()->createSprite(this, SOLDIERDIERIGHT);
				}
				break;
			}
			default: {break;}
		}
	}
	
	/*	shooting functions
	*		parameters: direction angle, target object
	*/
	void fireBullet(CL_Angle angle, GameObject* obj) {
		if (CL_System::get_time() > atime+200) {
			walk = 0;
			v.x = 0;
			Bullet* bullet = new Bullet(GameObject::getNextID(),BULLET, angle, 400, this, pos);
			ObjectManager::instance()->registerObject(bullet);
			SoundEngine::instance()->playSound(this, "SingleRiffleFire", false);
			atime = CL_System::get_time();
		}
	}

private:
	CL_Pointf v;
	int walk;
	size_t atime;
	size_t die_time;
	bool die_sound;
};


#endif
