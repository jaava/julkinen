/*
 * c++ course project triplane 2
 * world.hh
 * creaded by Jaakko Vallinoja
 */
#ifndef CPP_PROJECT_WORLD
#define CPP_PROJECT_WORLD

#include "defines.h"
#include "graphicengine.h"
#include "gameobject.h"
#include "plane.h"
#include "airfield.h"
#include "terrain.h"
#include "soldier.h"
#include "input.h"
#include "pelican.h"
#include "barracks.h"
#include "graphicobject.h"
#include "aagun.h"
 
class World
{
public:
	World(CL_DisplayWindow window, const CL_ResourceManager& resources):
		window(window), resources(resources)
	{
		time = CL_System::get_time();
	}
	
	void run(const CL_String);
	
	//updates the gameobjects static variable dt (milliseconds)
	void update_time();
	
	void initializeLevel(const CL_String filename);
	
	
private:
	bool quit;
	CL_DisplayWindow window;
	CL_ResourceManager resources;
	size_t time;
	Plane* pl1;
	Plane* pl2;
	size_t sound_count;
};

#endif
