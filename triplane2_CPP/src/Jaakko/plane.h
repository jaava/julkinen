/*
 *testing object
 *
 */
#ifndef CPP_PROJECT_PLANE
#define CPP_PROJECT_PLANE
 
#include "defines.h"
#include "bullet.h"
#include "bomb.h"
#include "pelican.h"
#include "gameobject.h"
#include "soundengine.h"
#include "stats.h"

#include <cmath>
#include <algorithm>


class Plane: public GameObject {
public:
	Plane(size_t id, int type, int color, size_t sprite, CL_Pointf pos);
	
	
	void update(); 
	
	void doCollide(GameObject* obj); 
	void flipPlane();
	void destruction();
	void refill();
	
	void fireBullet();
	void dropBomb();
	void dropPelican();

	void setUp(bool par) {up = par;}
	void setDown(bool par) {down = par;}
	void setEngine(bool par) {engine = par;}
	void setGun(bool par) {gun = par;}
	void setUpsidedown(bool par) {
		if (CL_System::get_time() > ctime + 500 && is_active) {
			if (landed) {
				refill();
			}
			else if (upsidedown && velocity > 20) {
				upsidedown = false;
				if (faction == 1) {sprite_id = FLIPANIMATIONBACK1;}
				else {sprite_id = FLIPANIMATIONBACK2;}
			}
			else if (velocity > 20) {
				upsidedown = true;
				if (faction == 1) {sprite_id = FLIPANIMATION1;}
				else {sprite_id = FLIPANIMATION2;}
			}
			ctime = CL_System::get_time();
		}
	}
	void setPelican(bool par) {pelican = par;}
	
	void setBomb(bool par) {bomb = par;}
	int& getHp() {return hp;}
	CL_Pointf getv() {return v;}
	
	void setBase(GameObject* _base) {base = _base;}

	~Plane() {delete outline;}
	
	
private:
	float y_force;
	float x_force;
	float mass;
	float forwardForce;
	float lift;
	float airResistance;
	float gravity;
	float acceleration;
	float velocity;
	CL_Pointf v;
	bool engineRunning;
	bool up;
	bool down;
	bool engine;
	bool bomb;
	bool gun;
	bool pelican;
	bool upsidedown;
	bool isLanding;
	bool control;
	bool landed;
	bool startgame;
	size_t atime;
	size_t btime;
	size_t ctime;
	float fuel;
	int ammo;
	int bombs;
	int pelicans;
	int hp;
	int sound_id;
	GameObject* base;
};

#endif
