/*
 * world.cc
 * c++ course project triplane2
 *
 */
 
#include "world.h"
#include <cmath>

void World::run(const CL_String filename) {
		quit = false;
		//initialize game resources
		GraphicEngine::instance()->staticSprites();
		initializeLevel(filename);
	
		CL_InputDevice keyboard = window.get_ic().get_keyboard();
		Input::instance()->setPlayer1(pl1);
		Input::instance()->setPlayer2(pl2);
		Input::instance()->start_keyboard();
		
		while (!quit)
		{
			update_time();
			
			if(keyboard.get_keycode(CL_KEY_ESCAPE) == true) {
				CL_System::sleep(200);
				quit = true;
			}
				
			//update and collision check every object
			ObjectManager::instance()->update();
			ObjectManager::instance()->collide();
			
			//update graphics
			GraphicEngine::instance()->draw();
			CL_System::sleep(3);
			CL_KeepAlive::process();
			
			//updates sound every ten cycles			
			if(sound_count >= 10) {
				SoundEngine::instance()->update();
				sound_count = 0;
			}
			sound_count++;
		}
		
		//draws results and releases resources
		GraphicEngine::instance()->drawResult();
		CL_System::sleep(800);
		
}

void World::update_time() {
		size_t newtime = CL_System::get_time();
		GameObject::setTime(newtime-time);
		time = newtime;
}

/*
	takes xml file as a parameter.
	creates objects from the descriptions in the file
*/
void World::initializeLevel(const CL_String filename) {
	
	CL_File  file(filename, CL_File::open_existing, CL_File::access_read); //reads file 
	CL_DomDocument doc; 
	std::vector<CL_DomNode> nodeVec =  doc.load(file); // convert to vector of nodes
	AaGun* aagun = NULL;

	for(std::vector<CL_DomNode>::iterator iter = nodeVec.begin(); iter != nodeVec.end(); iter++)
	{
		//reads attributes
		CL_DomElement element = iter->to_element(); 
		const CL_String name = element.get_attribute("name"); 
		float posx = CL_StringHelp::text_to_float(element.get_attribute("pos_x")); 
		float posy = CL_StringHelp::text_to_float(element.get_attribute("pos_y"));
		posx += CL_StringHelp::text_to_float(element.get_attribute("pos_x_offset")); 
		posy += CL_StringHelp::text_to_float(element.get_attribute("pos_y_offset"));
		int type = CL_StringHelp::text_to_int(element.get_attribute("type"));
		int owner = CL_StringHelp::text_to_int(element.get_attribute("owner"));  
		//creates objects
		switch (type) {
			case TERRAIN: {
				Terrain* terrain = new Terrain(GameObject::getNextID(), TERRAIN, owner, name, CL_Pointf(posx, HEIGHT));
				ObjectManager::instance()->registerObject(terrain);
				break;
			}
			case BARRACKS: {
				Barracks* bar = new Barracks(GameObject::getNextID(), BARRACKS, owner, CL_Pointf(posx, posy));
				ObjectManager::instance()->registerObject(bar);
				break;
			}
			case PLANE: {
				if (owner == 1) {
					pl1 = new Plane(GameObject::getNextID(),PLANE, owner, PLANE1_SPRITE, CL_Pointf(posx, posy));
					ObjectManager::instance()->registerObject(pl1);
				}
				else if (owner == 2) {
					pl2 = new Plane(GameObject::getNextID(),PLANE, owner, PLANE2_SPRITE, CL_Pointf(posx, posy));
					ObjectManager::instance()->registerObject(pl2);
				}
				break;
			}
			case AIRFIELD: {
				Airfield* field = new Airfield(GameObject::getNextID(), AIRFIELD, owner , CL_Pointf(posx, posy));
				ObjectManager::instance()->registerObject(field);
				if (owner == 1) {
					pl1->setBase(field);
				}
				else if (owner == 2) {
					pl2->setBase(field);
				}
				break;
			}
			case GRAPHICOBJECT: {
				//in this case owner = sprite_id
				GraphicObject* go = new GraphicObject(GameObject::getNextID(), AIRFIELD, owner, CL_Pointf(posx, posy));
				ObjectManager::instance()->registerObject(go);
				break;
			}
			case AAGUN: {
				AaGun* gun = new AaGun(GameObject::getNextID(), AAGUN, owner, CL_Pointf(posx, posy));
				ObjectManager::instance()->registerObject(gun);
				aagun = gun;
				break;
			}
			case AABASE: {
				AaBase* base = new AaBase(GameObject::getNextID(), AABASE, owner, aagun, CL_Pointf(posx, posy));
				ObjectManager::instance()->registerObject(base);
				break;
			}
			default: {break;}
		}
	}
	//gives graphicengine and soundengine the players to follow
	GraphicEngine::instance()->setPlayers(pl1, pl2);
	SoundEngine::instance()->initPlayers(pl1, pl2);
}
